const express = require("express");
const fileUpload = require("express-fileupload");
const https = require("https");
const fs = require("fs");
const mysql = require("mysql");
const axios = require("axios");

// Certificate
const privateKey = fs.readFileSync(
  "/opt/psa/var/certificates/scfwDszFI",
  "utf8"
);
const certificate = fs.readFileSync(
  "/opt/psa/var/certificates/scfwDszFI",
  "utf8"
);

const credentials = {
  key: privateKey,
  cert: certificate,
};

const app = express();

app.use(express.json({ limit: "200mb" }));
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload());

app.use((_request, response, next) => {
  response.setHeader("Access-Control-Allow-Origin", "*");
  response.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  response.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, DELETE, PUT, OPTIONS"
  );
  next();
});

const conn = mysql.createConnection({
  host: "localhost",
  user: "verify",
  password: "Te1ng82*5",
  database: "cv_univ_checker",
});

conn.connect((error) => {
  if (error) throw error;
  console.log("Successfully connected to the database.");
});

const httpsServer = https.createServer(credentials, app);

app.get("/states", (request, response) => {
  conn.query(`SELECT id, state FROM states`, (error, result) => {
    if (error) {
      return response.status(500).send(error);
    }
    return response.status(200).send(result);
  });
});

app.get("/states/board", (request, response) => {
  conn.query(
    `SELECT state as id, state as state FROM verified_boards GROUP BY state`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/states/regular", (request, response) => {
  conn.query(
    `SELECT state as id, state as state FROM verified_regular_universities GROUP BY state`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/types", (request, response) => {
  conn.query(
    `SELECT type FROM verified_universities GROUP BY type`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/types/regular", (request, response) => {
  conn.query(
    `SELECT type FROM verified_regular_universities GROUP BY type`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/universities/all", (request, response) => {
  conn.query(
    `SELECT verified_universities.id, verified_universities.image, verified_universities.name, verified_universities.mode, verified_universities.type, verified_universities.number_of_programmes, verified_universities.programmes, states.state, states.zone FROM verified_universities LEFT JOIN states ON states.id = verified_universities.state_id WHERE verified_universities.mode = 'ONLINE' ORDER BY verified_universities.managed_by_us DESC`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/universities/all/distance", (request, response) => {
  conn.query(
    `SELECT verified_universities.id, verified_universities.image, verified_universities.name, verified_universities.mode, verified_universities.type, verified_universities.number_of_programmes, verified_universities.programmes, states.state, states.zone FROM verified_universities LEFT JOIN states ON states.id = verified_universities.state_id WHERE verified_universities.mode = 'DISTANCE' ORDER BY verified_universities.managed_by_us DESC`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/boards/all", (request, response) => {
  conn.query(
    `SELECT verified_boards.id, verified_boards.image, verified_boards.name, verified_boards.code, verified_boards.state FROM verified_boards`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/regulars/all", (request, response) => {
  conn.query(
    `SELECT verified_regular_universities.id, verified_regular_universities.image, verified_regular_universities.name, verified_regular_universities.type, verified_regular_universities.state, verified_regular_universities.year FROM verified_regular_universities`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/wes/all", (request, response) => {
  conn.query(
    `SELECT wes_universities.id, wes_universities.image, wes_universities.name FROM wes_universities`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/fake/all", (request, response) => {
  conn.query(
    `SELECT fake_univs.id, fake_univs.image, fake_univs.name FROM fake_univs`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/universities/name", (request, response) => {
  conn.query(
    `SELECT verified_universities.id, verified_universities.image, verified_universities.name, verified_universities.mode, verified_universities.type, verified_universities.number_of_programmes, verified_universities.programmes, states.state, states.zone FROM verified_universities LEFT JOIN states ON states.id = verified_universities.state_id WHERE verified_universities.name = '${request.query.name}' AND verified_universities.mode = 'ONLINE' ORDER BY verified_universities.managed_by_us DESC`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/universities/name/distance", (request, response) => {
  conn.query(
    `SELECT verified_universities.id, verified_universities.image, verified_universities.name, verified_universities.mode, verified_universities.type, verified_universities.number_of_programmes, verified_universities.programmes, states.state, states.zone FROM verified_universities LEFT JOIN states ON states.id = verified_universities.state_id WHERE verified_universities.name = '${request.query.name}' AND verified_universities.mode = 'DISTANCE' ORDER BY verified_universities.managed_by_us DESC`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/boards/name", (request, response) => {
  conn.query(
    `SELECT verified_boards.id, verified_boards.image, verified_boards.name, verified_boards.code, verified_boards.state FROM verified_boards WHERE verified_boards.name = '${request.query.name}'`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/regulars/name", (request, response) => {
  conn.query(
    `SELECT verified_regular_universities.id, verified_regular_universities.image, verified_regular_universities.name, verified_regular_universities.type, verified_regular_universities.state, verified_regular_universities.year FROM verified_regular_universities WHERE verified_regular_universities.name = '${request.query.name}'`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/wes/name", (request, response) => {
  conn.query(
    `SELECT wes_universities.id, wes_universities.image, wes_universities.name FROM wes_universities WHERE wes_universities.name = '${request.query.name}'`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.get("/fake/name", (request, response) => {
  conn.query(
    `SELECT fake_univs.id, fake_univs.image, fake_univs.name FROM fake_univs WHERE fake_univs.name = '${request.query.name}'`,
    (error, result) => {
      if (error) {
        return response.status(500).send(error);
      }
      return response.status(200).send(result);
    }
  );
});

app.post("/universities/filtered", (request, response) => {
  const modes = JSON.parse(request.body.modes);
  const states = JSON.parse(request.body.states);
  const types = JSON.parse(request.body.types);
  const zones = JSON.parse(request.body.zones);

  var queryFilters = "";
  if (modes.length > 0) {
    queryFilters =
      queryFilters +
      `mode IN ${JSON.stringify(modes)
        .replace("[", "(")
        .replace("]", ")")} AND `;
  }
  if (states.length > 0) {
    queryFilters =
      queryFilters +
      `state_id IN ${JSON.stringify(states)
        .replace("[", "(")
        .replace("]", ")")} AND `;
  }
  if (types.length > 0) {
    queryFilters =
      queryFilters +
      `type IN ${JSON.stringify(types)
        .replace("[", "(")
        .replace("]", ")")} AND `;
  }
  if (zones.length > 0) {
    queryFilters =
      queryFilters +
      `zone IN ${JSON.stringify(zones)
        .replace("[", "(")
        .replace("]", ")")} AND `;
  }

  queryFilters = queryFilters.substr(0, queryFilters.length - 4);

  const finalQuery = `SELECT verified_universities.id, verified_universities.image, verified_universities.name, verified_universities.mode, verified_universities.type, verified_universities.number_of_programmes, verified_universities.programmes, states.state, states.zone FROM verified_universities LEFT JOIN states ON states.id = verified_universities.state_id WHERE ${queryFilters} AND verified_universities.mode = 'ONLINE' ORDER BY verified_universities.managed_by_us DESC`;

  conn.query(finalQuery, (error, result) => {
    if (error) {
      return response.status(500).send(error);
    }
    return response.status(200).send(result);
  });
});

app.post("/universities/filtered/distance", (request, response) => {
  const modes = JSON.parse(request.body.modes);
  const states = JSON.parse(request.body.states);
  const types = JSON.parse(request.body.types);
  const zones = JSON.parse(request.body.zones);

  var queryFilters = "";
  if (modes.length > 0) {
    queryFilters =
      queryFilters +
      `mode IN ${JSON.stringify(modes)
        .replace("[", "(")
        .replace("]", ")")} AND `;
  }
  if (states.length > 0) {
    queryFilters =
      queryFilters +
      `state_id IN ${JSON.stringify(states)
        .replace("[", "(")
        .replace("]", ")")} AND `;
  }
  if (types.length > 0) {
    queryFilters =
      queryFilters +
      `type IN ${JSON.stringify(types)
        .replace("[", "(")
        .replace("]", ")")} AND `;
  }
  if (zones.length > 0) {
    queryFilters =
      queryFilters +
      `zone IN ${JSON.stringify(zones)
        .replace("[", "(")
        .replace("]", ")")} AND `;
  }

  queryFilters = queryFilters.substr(0, queryFilters.length - 4);

  const finalQuery = `SELECT verified_universities.id, verified_universities.image, verified_universities.name, verified_universities.mode, verified_universities.type, verified_universities.number_of_programmes, verified_universities.programmes, states.state, states.zone FROM verified_universities LEFT JOIN states ON states.id = verified_universities.state_id WHERE ${queryFilters} AND verified_universities.mode = 'DISTANCE' ORDER BY verified_universities.managed_by_us DESC`;

  conn.query(finalQuery, (error, result) => {
    if (error) {
      return response.status(500).send(error);
    }
    return response.status(200).send(result);
  });
});

app.post("/boards/filtered", (request, response) => {
  const states = JSON.parse(request.body.states);

  var queryFilters = "";
  if (states.length > 0) {
    queryFilters =
      queryFilters +
      `verified_boards.state IN ${JSON.stringify(states)
        .replace("[", "(")
        .replace("]", ")")} AND `;
  }

  queryFilters = queryFilters.substr(0, queryFilters.length - 4);

  const finalQuery = `SELECT verified_boards.id, verified_boards.image, verified_boards.name, verified_boards.code, verified_boards.state FROM verified_boards WHERE ${queryFilters}`;
  conn.query(finalQuery, (error, result) => {
    if (error) {
      return response.status(500).send(error);
    }
    return response.status(200).send(result);
  });
});

app.post("/regulars/filtered", (request, response) => {
  const states = JSON.parse(request.body.states);
  const types = JSON.parse(request.body.types);

  var queryFilters = "";
  if (states.length > 0) {
    queryFilters =
      queryFilters +
      `verified_regular_universities.state IN ${JSON.stringify(states)
        .replace("[", "(")
        .replace("]", ")")} AND `;
  }
  if (types.length > 0) {
    queryFilters =
      queryFilters +
      `verified_regular_universities.type IN ${JSON.stringify(types)
        .replace("[", "(")
        .replace("]", ")")} AND `;
  }

  queryFilters = queryFilters.substr(0, queryFilters.length - 4);

  const finalQuery = `SELECT verified_regular_universities.id, verified_regular_universities.image, verified_regular_universities.name, verified_regular_universities.type, verified_regular_universities.year, verified_regular_universities.state FROM verified_regular_universities WHERE ${queryFilters}`;
  conn.query(finalQuery, (error, result) => {
    if (error) {
      return response.status(500).send(error);
    }
    return response.status(200).send(result);
  });
});

app.post("/issue", (request, response) => {
  const name = request.body.name;
  const number = request.body.number;
  const email = request.body.email;
  const issue = request.body.message;

  const htmlContent = `<html>
                    <head></head>
                    <body>
                        <h2>AN ISSUE HAS BEEN RAISED ON CV VERIFY PORTAL</h2>
                        <p style="margin: 0px"><b>Raised By:</b></p>
                        <p style="margin: 0px">${name} (${number}) (${email})</p>
                        <p style="margin: 0px"><b>Issue:</b></p>
                        <p style="margin: 0px">${issue}</p>
                    </body>
                </html>`;

  const sender = {
    name: "CV Verify",
    email: "no-reply@cvverify.com",
  };

  const to = [
    {
      name: "Akshay Sharma",
      email: "www.ghost25@gmail.com",
    },
    {
      name: "Sarthak Garg",
      email: "sarthakgarg070@gmail.com",
    },
    {
      name: "Rohit Gupta",
      email: "rohitgupta25@gmail.com",
    },
    {
      name: "Deepak",
      email: "deepaks@collegevidya.com",
    },
  ];

  const subject = `Issue raised on CV Verify Portal on ${Date.now()}`;

  var data = JSON.stringify({
    sender: sender,
    to: to,
    subject: subject,
    htmlContent: htmlContent,
  });

  var config = {
    method: "post",
    url: "https://api.sendinblue.com/v3/smtp/email/",
    headers: {
      Accept: "application/json",
      "api-key":
        "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",
      "Content-Type": "application/json",
    },
    data: data,
  };

  axios(config);
  response.status(200).send({ message: "success" });
});

httpsServer.listen(5432, () => {
  console.log("Server is running on port " + 5432);
});
