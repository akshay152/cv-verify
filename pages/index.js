import React, { useRef } from "react";
import Head from "next/head";
import { useEffect } from "react";
import Badge from "react-bootstrap/Badge";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import Modal from "react-bootstrap/Modal";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Popover from "react-bootstrap/Popover";
import Spinner from "react-bootstrap/Spinner";
import { ReactSearchAutocomplete } from "react-search-autocomplete";
import {
  BsFillEnvelopeFill,
  BsFillTelephoneFill,
  BsFilter,
  BsSearch,
} from "react-icons/bs";
import Image from "next/image";
import {
  FcGlobe,
  FcRules,
  FcMenu,
  FcGraduationCap,
  FcLandscape,
  FcAlphabeticalSortingZa,
  FcInfo,
  FcReading,
  FcLibrary,
} from "react-icons/fc";
import { FaTimes, FaChevronLeft, FaChevronRight } from "react-icons/fa";
import Link from "next/link";
import ReactPaginate from "react-paginate";
import { generate } from "shortid";
import { useState, Downgraded } from "@hookstate/core";
import { TiInfoOutline } from "react-icons/ti";
import { TabView, TabPanel } from "primereact/tabview";
import ModeInfoModal from "../components/modals/ModeInfoModal";

import {
  filterByBoardName,
  filterByFakeName,
  filterByName,
  filterByNameDistance,
  filterByRegularName,
  filterByWESName,
  getAllFakeUniversities,
  getAllRegularUniversities,
  getAllUniversities,
  getAllUniversitiesDistance,
  getAllVerifiedBoards,
  getAllWESUniversities,
  getFilteredBoards,
  getFilteredRegulars,
  getFilteredUniversities,
  getFilteredUniversitiesDistance,
  getStates,
  getStatesBoard,
  getStatesRegular,
  getTypes,
  getTypesRegular,
} from "./services/Misc";
import store from "../utils/store";
import AllCoursesModal from "../components/modals/AllCoursesModal";
import TypeInfoModal from "../components/modals/TypeInfoModal";
import SearchCard from "../components/BoardPanel/SearchCard";
import RegularSearchCard from "../components/RegularPanel/RegularSearchCard";
import WesSearchCard from "../components/WesPanel/WesSearchCard";
import TabOnline from "../components/BoardPanel/TabNames/TabOnline";
import TabBoard from "../components/BoardPanel/TabNames/TabBoard";
import TabRegular from "../components/BoardPanel/TabNames/TabRegular";
import TabWes from "../components/BoardPanel/TabNames/TabWes";
import styles from "../styles/index.module.css";
import ReportIssue from "../components/modals/ReportIssue";
import ReportIssueComponent from "../components/ReportIssueComponent";
import SubmitContent from "../components/modals/SubmitContent";
import TabFake from "../components/BoardPanel/TabNames/TabFake";
import TabDistance from "../components/BoardPanel/TabNames/TabDistance";
import Banner from "../components/global/Banner";
import BelowBanner from "../components/global/BelowBanner";
import { getHomePageUniversityData, getMenu } from "../services/misc";
import KeyPoints from "../components/global/KeyPoints";
import OurExpert from "../components/OurExpert";
import Footer from "../components/global/Footer";

export default function Home() {
  const zones = ["EAST", "WEST", "NORTH", "SOUTH"];
  const modes = [
    {
      name: "ONLINE",
      description: "Exams and Mode of education will completely be online.",
    },
    {
      name: "DISTANCE",
      description:
        "Exams would happen in university campus but mode of education would be online.",
    },
  ];

  const [states, setStates] = React.useState([]);
  const [stateCopy, setStateCopy] = React.useState([]);

  const [statesBoard, setStatesBoard] = React.useState([]);
  const [stateCopyBoard, setStateCopyBoard] = React.useState([]);

  const [statesRegular, setStatesRegular] = React.useState([]);
  const [stateCopyRegular, setStateCopyRegular] = React.useState([]);

  const [types, setTypes] = React.useState([]);
  const [typesRegular, setTypesRegular] = React.useState([]);

  const globalStore = useState(store);

  const [searching, setSearching] = React.useState(true);
  const [filterStates, setFilterStates] = React.useState(false);

  const [showAllStates, setShowAllStates] = React.useState(false);

  //Filter arrays
  const [selectedModes, setSelectedModes] = React.useState([]);
  const [selectedStates, setSelectedStates] = React.useState([]);
  const [selectedTypes, setSelectedTypes] = React.useState([]);
  const [selectedZones, setSelectedZones] = React.useState([]);

  const [selectedModesDistance, setSelectedModesDistance] = React.useState([]);
  const [selectedStatesDistance, setSelectedStatesDistance] = React.useState(
    []
  );
  const [selectedTypesDistance, setSelectedTypesDistance] = React.useState([]);
  const [selectedZonesDistance, setSelectedZonesDistance] = React.useState([]);

  const [selectedStatesBoard, setSelectedStatesBoard] = React.useState([]);
  const [selectedStatesRegular, setSelectedStatesRegular] = React.useState([]);

  const [selectedTypesRegular, setSelectedTypesRegular] = React.useState([]);

  const [reqTime, setReqTime] = React.useState(0);

  const stateTextRef = useRef(null);

  const [universityListData, setUniversityListData] = React.useState([]);
  const [menuListData, setMenuListData] = React.useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        const universityData = await getHomePageUniversityData();
        setUniversityListData(universityData.data.data);
      } catch (error) {
        console.error("Error fetching university data", error);
      }
    }
    fetchData();
  }, []);

  useEffect(() => {
    async function menuData() {
      try {
        const menuListData = await getMenu();
        setMenuListData(menuListData.data);
      } catch (error) {
        console.error("Error in menu data", error);
      }
    }
    menuData();
  }, []);

  useEffect(() => {
    getStates().then((response) => {
      setStates(response.data);
      setStateCopy(response.data);
    });
    getTypes().then((response) => setTypes(response.data));
    getTypesRegular().then((response) => setTypesRegular(response.data));

    getStatesBoard().then((response) => {
      setStatesBoard(response.data);
      setStateCopyBoard(response.data);
    });

    getStatesRegular().then((response) => {
      setStatesRegular(response.data);
      setStateCopyRegular(response.data);
    });

    getAllUnivs();
    getAllUnivsDistance();
    getAllBoards();
    getAllRegular();
    getAllWES();
    getAllFake();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function getAllUnivs() {
    setSearching(true);
    getAllUniversities().then((response) => {
      globalStore.universities.set(response.data);
      globalStore.universitiesCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);
    });
  }

  function getAllUnivsDistance() {
    setSearching(true);
    getAllUniversitiesDistance().then((response) => {
      globalStore.universitiesDistance.set(response.data);
      globalStore.universitiesDistanceCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);
    });
  }

  function getAllBoards() {
    setSearching(true);
    getAllVerifiedBoards().then((response) => {
      globalStore.boards.set(response.data);
      globalStore.boardsCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);
    });
  }

  function getAllRegular() {
    setSearching(true);
    getAllRegularUniversities().then((response) => {
      globalStore.regulars.set(response.data);
      globalStore.regularsCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);
    });
  }

  function getAllWES() {
    setSearching(true);
    getAllWESUniversities().then((response) => {
      globalStore.wes.set(response.data);
      globalStore.wesCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);
    });
  }

  function getAllFake() {
    setSearching(true);
    getAllFakeUniversities().then((response) => {
      globalStore.fake.set(response.data);
      globalStore.fakeCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);
    });
  }

  const handleStateText = (text) => {
    if (text === "") {
      setStateCopy(states);
    } else {
      setStateCopy(
        states.filter((state) =>
          state.state.toLowerCase().includes(text.toLowerCase())
        )
      );
    }
  };

  const handleStateTextDistance = (text) => {
    if (text === "") {
      setStateCopy(states);
    } else {
      setStateCopy(
        states.filter((state) =>
          state.state.toLowerCase().includes(text.toLowerCase())
        )
      );
    }
  };

  const handleStateTextBoard = (text) => {
    if (text === "") {
      setStateCopyBoard(statesBoard);
    } else {
      setStateCopyBoard(
        statesBoard.filter((state) =>
          state.state.toLowerCase().includes(text.toLowerCase())
        )
      );
    }
  };

  const handleStateTextRegular = (text) => {
    if (text === "") {
      setStateCopyRegular(statesRegular);
    } else {
      setStateCopyRegular(
        statesRegular.filter((state) =>
          state.state.toLowerCase().includes(text.toLowerCase())
        )
      );
    }
  };

  useEffect(() => {
    if (filterStates && stateTextRef.current) {
      stateTextRef.current.focus();
    }
  }, [filterStates]);

  //Online Pagination
  const [currentPage, setCurrentPage] = React.useState(0);
  const [universitiesPerPage] = React.useState(9);

  const pagesVisited = currentPage * universitiesPerPage;
  const pageCount = Math.ceil(
    globalStore.universitiesCopy.attach(Downgraded).get().length /
      universitiesPerPage
  );

  const changePage = ({ selected }) => {
    setCurrentPage(selected);
  };

  //Distance Pagination
  const [currentPageDistance, setCurrentPageDistance] = React.useState(0);
  const [universitiesPerPageDistance] = React.useState(9);

  const pagesVisitedDistance =
    currentPageDistance * universitiesPerPageDistance;
  const pageCountDistance = Math.ceil(
    globalStore.universitiesDistanceCopy.attach(Downgraded).get().length /
      universitiesPerPageDistance
  );

  const changePageDistance = ({ selected }) => {
    setCurrentPageDistance(selected);
  };

  //Boards Pagination
  const [currentPageBoard, setCurrentPageBoard] = React.useState(0);
  const [boardsPerPage] = React.useState(20);

  const pagesVisitedBoard = currentPageBoard * boardsPerPage;
  const pageCountBoard = Math.ceil(
    globalStore.boardsCopy.attach(Downgraded).get().length / boardsPerPage
  );

  const changePageBoard = ({ selected }) => {
    setCurrentPageBoard(selected);
  };

  //Regular Pagination
  const [currentPageRegular, setCurrentPageRegular] = React.useState(0);
  const [regularsPerPage] = React.useState(10);

  const pagesVisitedRegular = currentPageRegular * regularsPerPage;
  const pageCountRegular = Math.ceil(
    globalStore.regularsCopy.attach(Downgraded).get().length / regularsPerPage
  );

  const changePageRegular = ({ selected }) => {
    setCurrentPageRegular(selected);
  };

  //WES Pagination
  const [currentPageWES, setCurrentPageWES] = React.useState(0);
  const [WESPerPage] = React.useState(20);

  const pagesVisitedWES = currentPageWES * WESPerPage;
  const pageCountWES = Math.ceil(
    globalStore.wesCopy.attach(Downgraded).get().length / WESPerPage
  );

  const changePageWES = ({ selected }) => {
    setCurrentPageWES(selected);
  };

  //Fake Pagination
  const [currentPageFake, setCurrentPageFake] = React.useState(0);
  const [FakePerPage] = React.useState(20);

  const pagesVisitedFake = currentPageFake * FakePerPage;
  const pageCountFake = Math.ceil(
    globalStore.fakeCopy.attach(Downgraded).get().length / FakePerPage
  );

  const changePageFake = ({ selected }) => {
    setCurrentPageFake(selected);
  };

  const handleOnSelect = (item) => {
    setSearching(true);
    filterByName(item.name).then((response) => {
      globalStore.universitiesCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);

      setCurrentPage(0);
    });
  };

  const handleOnSelectDistance = (item) => {
    setSearching(true);
    filterByNameDistance(item.name).then((response) => {
      globalStore.universitiesDistanceCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);

      setCurrentPage(0);
    });
  };

  const handleOnSelectBoard = (item) => {
    setSearching(true);
    filterByBoardName(item.name).then((response) => {
      globalStore.boardsCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);

      setCurrentPageBoard(0);
    });
  };

  const handleOnSelectRegular = (item) => {
    setSearching(true);
    filterByRegularName(item.name).then((response) => {
      globalStore.regularsCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);

      setCurrentPageRegular(0);
    });
  };

  const handleOnSelectWES = (item) => {
    setSearching(true);
    filterByWESName(item.name).then((response) => {
      globalStore.wesCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);

      setCurrentPageWES(0);
    });
  };

  const handleOnSelectFake = (item) => {
    setSearching(true);
    filterByFakeName(item.name).then((response) => {
      globalStore.fakeCopy.set(response.data);
      setReqTime(genRand(0.1, 0.9, 2));
      setSearching(false);

      setCurrentPageFake(0);
    });
  };

  const handleOnSearch = (string) => {
    if (string === "") {
      getAllUnivs();
    }
  };

  const handleOnSearchDistance = (string) => {
    if (string === "") {
      getAllUnivsDistance();
    }
  };

  const handleOnSearchBoards = (string) => {
    if (string === "") {
      getAllBoards();
    }
  };

  const handleOnSearchRegular = (string) => {
    if (string === "") {
      getAllRegular();
    }
  };

  const handleOnSearchWES = (string) => {
    if (string === "") {
      getAllWES();
    }
  };

  const handleOnSearchFake = (string) => {
    if (string === "") {
      getAllFake();
    }
  };

  useEffect(() => {
    if (
      selectedModes.length === 0 &&
      selectedStates.length === 0 &&
      selectedTypes.length === 0 &&
      selectedZones.length === 0
    ) {
      getAllUnivs();
    } else {
      var ss = selectedStates;
      var sz = selectedZones;

      if (selectedStates.length > 0) {
        sz = [];
        document
          .getElementsByName("zone-check")
          .forEach((el) => (el.checked = false));
      }

      if (selectedZones.length > 0) {
        ss = [];
        document
          .getElementsByName("state-check")
          .forEach((el) => (el.checked = false));
      }

      setSearching(true);
      getFilteredUniversities(selectedModes, ss, selectedTypes, sz).then(
        (response) => {
          globalStore.universitiesCopy.set(response.data);
          setReqTime(genRand(0.1, 0.9, 2));
          setSearching(false);
        }
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedModes, selectedStates, selectedTypes, selectedZones]);

  useEffect(() => {
    if (
      selectedModesDistance.length === 0 &&
      selectedStatesDistance.length === 0 &&
      selectedTypesDistance.length === 0 &&
      selectedZonesDistance.length === 0
    ) {
      getAllUnivsDistance();
    } else {
      var ss = selectedStatesDistance;
      var sz = selectedZonesDistance;

      if (selectedStatesDistance.length > 0) {
        sz = [];
        document
          .getElementsByName("zone-check-distance")
          .forEach((el) => (el.checked = false));
      }

      if (selectedZonesDistance.length > 0) {
        ss = [];
        document
          .getElementsByName("state-check-distance")
          .forEach((el) => (el.checked = false));
      }

      setSearching(true);
      getFilteredUniversitiesDistance(
        selectedModesDistance,
        ss,
        selectedTypesDistance,
        sz
      ).then((response) => {
        globalStore.universitiesDistanceCopy.set(response.data);
        setReqTime(genRand(0.1, 0.9, 2));
        setSearching(false);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    selectedModesDistance,
    selectedStatesDistance,
    selectedTypesDistance,
    selectedZonesDistance,
  ]);

  useEffect(() => {
    if (selectedStatesBoard.length === 0) {
      getAllBoards();
    } else {
      setSearching(true);
      getFilteredBoards(selectedStatesBoard).then((response) => {
        globalStore.boardsCopy.set(response.data);
        setReqTime(genRand(0.1, 0.9, 2));
        setSearching(false);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedStatesBoard]);

  useEffect(() => {
    if (
      selectedStatesRegular.length === 0 &&
      selectedTypesRegular.length === 0
    ) {
      getAllRegular();
    } else {
      setSearching(true);
      getFilteredRegulars(selectedStatesRegular, selectedTypesRegular).then(
        (response) => {
          globalStore.regularsCopy.set(response.data);
          setReqTime(genRand(0.1, 0.9, 2));
          setSearching(false);
        }
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedStatesRegular, selectedTypesRegular]);

  const badgeColor = [
    {
      tag: "CENTRAL UNIVERSITY",
      variant: "success",
    },
    {
      tag: "DEEMED TO BE UNIVERSITY",
      variant: "warning",
    },
    {
      tag: "PRIVATE UNIVERSITY",
      variant: "info",
    },
    {
      tag: "STATE OPEN UNIVERSITY",
      variant: "dark",
    },
    {
      tag: "STATE UNIVERSITY",
      variant: "secondary",
    },
  ];

  const coursesLinks = [
    {
      name: "BACHELOR OF ARTS",
      link: "https://collegevidya.com/courses/online-distance-ba/",
      count: 8,
    },
    {
      name: "BACHELOR OF BUSINESS ADMINISTRATION",
      link: "https://collegevidya.com/courses/online-distance-bba/",
      count: 15,
    },
    {
      name: "BACHELOR OF COMMERCE",
      link: "https://collegevidya.com/courses/online-distance-bcom/",
      count: 13,
    },
    {
      name: "BACHELOR OF COMPUTER APPLICATION",
      link: "https://collegevidya.com/courses/online-distance-bca/",
      count: 9,
    },
    {
      name: "MASTER OF BUSINESS ADMINISTRATION",
      link: "https://collegevidya.com/courses/online-distance-mba/",
      count: 18,
    },
    {
      name: "MASTER OF COMPUTER APPLICATION",
      link: "https://collegevidya.com/courses/online-distance-mca/",
      count: 8,
    },
    {
      name: "MASTER OF COMMERCE",
      link: "https://collegevidya.com/courses/online-distance-mcom/",
      count: 11,
    },
    {
      name: "MASTER OF ARTS",
      link: "https://collegevidya.com/courses/online-distance-ma/",
      count: 10,
    },
    {
      name: "MASTER OF SCIENCE",
      link: "https://collegevidya.com/courses/online-distance-msc/",
      count: 6,
    },
  ];

  const badgeColorRegular = [
    {
      tag: "Central University",
      variant: "success",
    },
    {
      tag: "Deemed University",
      variant: "warning",
    },
    {
      tag: "Private University",
      variant: "info",
    },
    {
      tag: "State Private University",
      variant: "dark",
    },
    {
      tag: "State University",
      variant: "secondary",
    },
  ];

  const [showCourseModal, setShowCourseModal] = React.useState(false);
  const [modalContent, setCourseModalContent] = React.useState(null);
  const [modalSize, setCourseModalSize] = React.useState("sm");

  const [showOtherModal, setShowOtherModal] = React.useState(false);
  const [otherModalContent, setOtherModalContent] = React.useState(null);

  const [showSubmitModal, setShowSubmitModal] = React.useState(false);
  const [submitModalContent, setSubmitModalContent] = React.useState(null);

  function CoursesModal(props) {
    return (
      <Modal {...props} size="md" scrollable={true} centered>
        {modalContent}
      </Modal>
    );
  }

  function OtherModal(props) {
    return (
      <Modal {...props} size={modalSize} scrollable={true} centered>
        {otherModalContent}
      </Modal>
    );
  }

  function SubmitModal(props) {
    return (
      <Modal {...props} size="sm" centered>
        {submitModalContent}
      </Modal>
    );
  }

  function setupModal(key, ekey = "") {
    if (key === "course") {
      setCourseModalContent(
        <AllCoursesModal
          data={ekey}
          coursesLinks={coursesLinks}
          showTypeModal={() => setupModal("type-info")}
          closeModal={() => setShowCourseModal(false)}
        />
      );
      setShowCourseModal(true);
    }
    if (key === "type-info") {
      setCourseModalSize("md");
      setOtherModalContent(
        <TypeInfoModal closeModal={() => setShowOtherModal(false)} />
      );
      setShowOtherModal(true);
    }
    if (key === "mode-info") {
      setCourseModalSize("lg");
      setOtherModalContent(
        <ModeInfoModal closeModal={() => setShowOtherModal(false)} />
      );
      setShowOtherModal(true);
    }
    if (key === "report-issue") {
      setOtherModalContent(
        <ReportIssue
          closeModal={() => {
            setShowOtherModal(false);
          }}
          endModal={() => {
            setShowOtherModal(false);
            setupModal("submit-issue");
          }}
        />
      );
      setShowOtherModal(true);
    }
    if (key === "submit-issue") {
      setSubmitModalContent(
        <SubmitContent closeModal={() => setShowSubmitModal(false)} />
      );
      setShowSubmitModal(true);
    }
  }

  function genRand(min, max, decimalPlaces) {
    var rand =
      Math.random() < 0.5
        ? (1 - Math.random()) * (max - min) + min
        : Math.random() * (max - min) + min; // could be min or max or anything in between
    var power = Math.pow(10, decimalPlaces);
    return Math.floor(rand * power) / power;
  }

  return (
    <div>
      <Head>
        <title>University Eligibility Tool - College Vidya</title>
      </Head>
      <section>
        <Container className="my-3 my-lg-5">
          <Banner />
          <BelowBanner UniversityData={universityListData} />
          {/* <KeyPoints /> */}
        </Container>
        {/* <Container fluid className="mx-auto">
          <div className="d-flex justify-content-center py-md-5 py-sm-1 my-3 px-4 flex-column">
            <h2
              style={{ fontSize: "60px" }}
              className={`${styles.banner_header} fw-bold text-center`}
            >
              <AnimatedText texts={["Cross Check", "Verify"]} />
              <div className="caption">
                your{" "}
                <span style={{ color: "#0074d7" }}>
                  {globalStore.activeIndex.get() === 3
                    ? "Board"
                    : globalStore.activeIndex.get() === 2
                    ? "WES approval"
                    : "University"}
                </span>{" "}
                before applying! <br />{" "}
              </div>
            </h2>
            <div
              className="d-flex align-items-center mb-2 mt-2 justify-content-center"
              style={{ color: "#fd4705", gap: "6px" }}
            >
              <MdWarningAmber fontSize={20} />
              Don&apos;t make a decision that you might regret!
            </div>
            <div
              className="d-flex align-items-center mb-2 justify-content-center"
              style={{ color: "#fd4705", gap: "6px" }}
            >
              <BiCheckShield fontSize={20} />
              Pick UGC-DEB stamped universities only
            </div>
            <Link href="https://www.youtube.com/watch?v=Diao6b6lfpE" passHref>
              <a
                target="_blank"
                className="shadow-none d-flex align-items-center px-4 py-2 mt-4"
                style={{
                  width: "fit-content",
                  fontSize: "14px",
                  gap: "8px",
                  backgroundColor: "#cce7ff",
                  color: "#0074d7",
                  borderRadius: "25px",
                  border: "1px solid #cce7ff",
                  alignSelf: "center",
                  textDecoration: "none",
                }}
              >
                <FaEye color="#0074d7" fontSize={18} />
                Watch Video
              </a>
            </Link>
          </div>
        </Container> */}
      </section>

      <section className="py-5" style={{ backgroundColor: "#fafafa" }}>
        <Container>
          <div className="text-center mb-4">
            {" "}
            <p className="h2 fw-bold">Cross Check your university </p>
          </div>
          <TabView
            className="tab_wrap"
            activeIndex={globalStore.activeIndex.get()}
            onTabChange={(e) => globalStore.activeIndex.set(e.index)}
          >
            <TabPanel header={<TabOnline />}>
              <Row>
                <Col md={3} className="d-none d-lg-block position-relative">
                  <div className="px-3 py-3">
                    {" "}
                    <p
                      className="fw-bold d-flex align-items-center"
                      style={{ fontSize: "18px", gap: "6px" }}
                    >
                      <BsFilter /> Online Filters
                    </p>
                    <div
                      className="bg-white mb-3 shadow-2 p-4"
                      style={{
                        position: "relative",
                        padding: "8px",
                        borderRadius: "5px",
                      }}
                    >
                      {showAllStates ? (
                        <div className="categoery_modal">
                          <div className="position-relative h-100">
                            <div className="px-3 border-bottom">
                              <Row className="align-items-center">
                                <Col className="text-end">
                                  <span
                                    onClick={() => setShowAllStates(false)}
                                    className="categ_modal_close"
                                  >
                                    &times;
                                  </span>
                                </Col>
                              </Row>
                            </div>
                            <div className="cate_modal_body">
                              {stateCopy
                                .slice(8, stateCopy.length)
                                .map((state) => (
                                  <div key={state.id}>
                                    <label>
                                      <Form.Check
                                        name="state-check"
                                        defaultChecked={selectedStates.includes(
                                          state.id.toString()
                                        )}
                                        value={state.id}
                                        onChange={(e) => {
                                          if (e.target.checked) {
                                            setSelectedStates((states) => [
                                              ...states,
                                              e.target.value,
                                            ]);
                                          } else {
                                            setSelectedStates(
                                              selectedStates.filter(
                                                (state) =>
                                                  state !== e.target.value
                                              )
                                            );
                                          }
                                        }}
                                      />
                                      {state.state}
                                    </label>
                                  </div>
                                ))}
                            </div>
                          </div>
                        </div>
                      ) : null}
                      <div className="d-flex justify-content-between">
                        {filterStates ? (
                          <div className="w-100 mb-1">
                            <InputGroup>
                              <FormControl
                                ref={stateTextRef}
                                className="shadow-none"
                                onChange={(e) =>
                                  handleStateText(e.target.value)
                                }
                              />
                              <InputGroup.Text
                                onClick={() => {
                                  setFilterStates(false);
                                  setStateCopy(states);
                                }}
                                style={{ cursor: "pointer" }}
                              >
                                <FaTimes color="white" />{" "}
                              </InputGroup.Text>
                            </InputGroup>
                          </div>
                        ) : (
                          <>
                            <p
                              className="fw-bold d-flex align-items-center"
                              style={{ gap: "6px" }}
                            >
                              <FcAlphabeticalSortingZa className="d-none" />
                              States
                            </p>
                            <div
                              onClick={() => setFilterStates(true)}
                              className="d-flex justify-content-center align-items-center"
                              style={{
                                borderRadius: "50%",
                                height: "30px",
                                width: "30px",
                                backgroundColor: "#f5f5f6",
                                cursor: "pointer",
                              }}
                            >
                              <BsSearch />
                            </div>
                          </>
                        )}
                      </div>
                      <div style={{ maxHeight: "200px", overflowY: "scroll" }}>
                        {stateCopy.map((state) => (
                          <span key={state.id}>
                            <Form.Check
                              name="state-check"
                              defaultChecked={selectedStates.includes(
                                state.id.toString()
                              )}
                              label={state.state}
                              value={state.id}
                              onChange={(e) => {
                                if (e.target.checked) {
                                  setSelectedStates((states) => [
                                    ...states,
                                    e.target.value,
                                  ]);
                                } else {
                                  setSelectedStates(
                                    selectedStates.filter(
                                      (state) => state !== e.target.value
                                    )
                                  );
                                }
                              }}
                            />
                          </span>
                        ))}
                        {/* <div className='mt-1 mb-1 text-left ps-4'>
                    <span onClick={() => setShowAllStates(true)} style={{ fontSize: "14px", color: "#0074d7", cursor: "pointer" }}>View All</span>
                  </div> */}
                      </div>
                    </div>
                    <div
                      className="bg-white mb-3 shadow-2 p-4"
                      style={{ padding: "8px", borderRadius: "5px" }}
                    >
                      <div className="d-flex justify-content-between">
                        <p
                          className="fw-bold d-flex align-items-center"
                          style={{ gap: "6px" }}
                        >
                          <FcGraduationCap className="d-none" /> Types
                        </p>
                        <FcInfo
                          onClick={() => setupModal("type-info")}
                          fontSize={18}
                          style={{ cursor: "pointer" }}
                        />
                      </div>
                      <div>
                        {types.map((type) => (
                          <span key={type.type}>
                            <Form.Check
                              name="type-check"
                              label={type.type}
                              value={type.type}
                              defaultChecked={selectedTypes.includes(
                                type.type.toString()
                              )}
                              onChange={(e) => {
                                if (e.target.checked) {
                                  setSelectedTypes((types) => [
                                    ...types,
                                    e.target.value,
                                  ]);
                                } else {
                                  setSelectedTypes(
                                    selectedTypes.filter(
                                      (type) => type !== e.target.value
                                    )
                                  );
                                }
                              }}
                            />
                          </span>
                        ))}
                      </div>
                    </div>
                    <div
                      className="bg-white shadow-2 p-4"
                      style={{ padding: "8px", borderRadius: "5px" }}
                    >
                      <div className="d-flex justify-content-between">
                        <p
                          className="fw-bold d-flex align-items-center mb-3"
                          style={{ gap: "8px" }}
                        >
                          <FcLandscape className="d-none" />
                          Zones
                        </p>
                      </div>
                      <div>
                        {zones.map((zone) => (
                          <span key={zone}>
                            <Form.Check
                              name="zone-check"
                              label={zone}
                              value={zone}
                              defaultChecked={selectedZones.includes(
                                zone.toString()
                              )}
                              onChange={(e) => {
                                if (e.target.checked) {
                                  setSelectedZones((zones) => [
                                    ...zones,
                                    e.target.value,
                                  ]);
                                } else {
                                  setSelectedZones(
                                    selectedZones.filter(
                                      (zones) => zones !== e.target.value
                                    )
                                  );
                                }
                              }}
                            />
                          </span>
                        ))}
                      </div>
                    </div>
                  </div>
                </Col>
                <Col lg={9}>
                  <Row className="justify-content-end mb-2">
                    <Col lg={12} style={{ zIndex: 999 }}>
                      <ReactSearchAutocomplete
                        onSearch={handleOnSearch}
                        onClear={() => getAllUnivs()}
                        placeholder="Is your Online University Valid?"
                        onSelect={handleOnSelect}
                        items={globalStore.universities
                          .attach(Downgraded)
                          .get()}
                        formatResult={(item) => {
                          return (
                            <>
                              <span
                                style={{
                                  display: "flex",
                                  textAlign: "left",
                                  alignItems: "center",
                                  gap: "6px",
                                }}
                              >
                                {item.name}
                                <Badge
                                  style={{ fontSize: "12px" }}
                                  bg={
                                    item.mode === "ONLINE"
                                      ? "primary"
                                      : "warning"
                                  }
                                >
                                  {item.mode}
                                </Badge>
                              </span>
                            </>
                          );
                        }}
                      />
                      <span
                        className="ps-3 mt-3 text-success fw-bold d-block"
                        style={{ fontSize: "12px" }}
                      >
                        About{" "}
                        {
                          globalStore.universitiesCopy.attach(Downgraded).get()
                            .length
                        }{" "}
                        results ({reqTime} seconds)
                      </span>
                    </Col>
                  </Row>

                  {searching ? (
                    <div
                      className="d-flex justify-content-center align-items-center"
                      style={{ height: "300px", color: "#0074d7" }}
                    >
                      <Spinner animation="border" size="lg" />
                    </div>
                  ) : globalStore.universitiesCopy.length === 0 ? (
                    <div
                      style={{
                        borderRadius: "8px",
                        padding: "16px",
                        backgroundColor: "white",
                        flexDirection: "column",
                      }}
                      className="d-flex align-items-center justify-content-center mb-5"
                    >
                      <h4>No results found!</h4>
                      <p className="text-center" style={{ fontSize: "14px" }}>
                        The university you are looking for may not be approved
                        by UGC for online education. <br /> Contact our experts
                        for more details
                      </p>
                      <Link href="https://collegevidya.com/suggest-me-an-university">
                        <a target="_blank" className="suggest-me-btn">
                          <Image
                            src="/images/badge.png"
                            width={20}
                            height={20}
                            alt="badge"
                          />
                          Suggest Me Approved University in 2 mins
                        </a>
                      </Link>
                      <ReportIssueComponent
                        openModal={() => setupModal("report-issue")}
                      />
                    </div>
                  ) : (
                    <Row className="mt-3 row-cols-1 row-cols-md-2 row-cols-lg-2 row-cols-xl-3">
                      <>
                        {globalStore.universitiesCopy
                          .attach(Downgraded)
                          .get()
                          .slice(
                            pagesVisited,
                            pagesVisited + universitiesPerPage
                          )
                          .map((university) => (
                            <Col key={university.id} className="mb-4">
                              <div
                                className="bg-white mb-3 h-100 shadow-2 overflow-hidden"
                                style={{ borderRadius: "16px" }}
                              >
                                <>
                                  <div
                                    className="text-center d-flex justify-content-center align-items-start"
                                    md={3}
                                    style={{
                                      borderRadius: "10px",
                                      backgroundColor: "#FFF",
                                    }}
                                  >
                                    {university.image &&
                                    university.image !== "" ? (
                                      <div
                                        className="univ-logo"
                                        style={{
                                          borderRadius: "8px",
                                          overflow: "hidden",
                                        }}
                                      >
                                        <Image
                                          className="rounded"
                                          src={university.image}
                                          width={306}
                                          height={222}
                                          alt="University Image"
                                          layout="responsive"
                                        />
                                      </div>
                                    ) : (
                                      <Image
                                        className="rounded"
                                        src="/images/university_placeholder.png"
                                        height={150}
                                        width={150}
                                        alt="University Image"
                                      />
                                    )}
                                  </div>
                                  <div className="px-3 pt-3">
                                    <div className={`text-truncate`}>
                                      <Link
                                        href="https://collegevidya.com/suggest-me-an-university/"
                                        passHref
                                      >
                                        <a
                                          target="_blank"
                                          style={{
                                            gap: "6px",
                                            textDecoration: "none",
                                            color: "black",
                                          }}
                                        >
                                          <div style={{ fontSize: "16px" }}>
                                            <span
                                              style={{ fontWeight: "bold" }}
                                            >
                                              {university.name}
                                            </span>
                                          </div>
                                        </a>
                                      </Link>
                                      <Badge
                                        className="mt-2 rounded-pill bg-light text-dark fw-normal"
                                        onClick={() => setupModal("type-info")}
                                        style={{
                                          fontSize: "11px",
                                          cursor: "pointer",
                                        }}
                                      >
                                        {university.type}
                                      </Badge>
                                    </div>
                                    {/* <div
                                      className="d-flex align-items-center mb-2"
                                      style={{ gap: "6px" }}
                                    >
                                      <OverlayTrigger
                                        trigger="click"
                                        placement="right"
                                        overlay={
                                          <Popover id="popover-basic">
                                            <Popover.Body>
                                              {
                                                modes.filter(
                                                  (mode) =>
                                                    mode.name ===
                                                    university.mode
                                                )[0].description
                                              }
                                            </Popover.Body>
                                          </Popover>
                                        }
                                      >
                                        <span
                                          className="d-flex align-items-center"
                                          style={{
                                            cursor: "pointer",
                                            gap: "6px",
                                          }}
                                        >
                                          <Badge
                                            className="mt-1 mb-1"
                                            bg={
                                              university.mode === "ONLINE"
                                                ? "primary"
                                                : "warning"
                                            }
                                          >
                                            {university.mode}
                                          </Badge>
                                          <TiInfoOutline
                                            color={
                                              university.mode === "ONLINE"
                                                ? "#ffc107"
                                                : "red"
                                            }
                                          />
                                        </span>
                                      </OverlayTrigger>
                                    </div> */}
                                    <div
                                      className="d-flex align-items-center mb-1 mt-2 py-2"
                                      style={{
                                        color: "#535766",
                                        gap: "6px",
                                        borderBottom: "1px solid #eee",
                                      }}
                                    >
                                      <FcReading fontSize={18} />
                                      <div>
                                        Mode of education/exam :{" "}
                                        <span className="fw-bold text-success">
                                          {" "}
                                          {university.mode}
                                        </span>
                                      </div>
                                    </div>
                                    <div
                                      className="d-flex align-items-center mb-1 py-2 text-lowercase"
                                      style={{
                                        color: "#535766",
                                        gap: "6px",
                                        borderBottom: "1px solid #eee",
                                      }}
                                    >
                                      <FcGlobe fontSize={18} />
                                      <div>
                                        {university.state} ({university.zone})
                                      </div>
                                    </div>
                                    <div
                                      className="mb-1 d-flex align-items-center py-2"
                                      style={{
                                        fontSize: "14px",
                                        color: "#535766",
                                        gap: "6px",
                                      }}
                                    >
                                      <FcRules fontSize={18} />{" "}
                                      <span>
                                        Programmes:{" "}
                                        <b>{university.number_of_programmes}</b>
                                      </span>
                                    </div>
                                    <ul
                                      className="d-none"
                                      style={{
                                        paddingLeft: "0px",
                                        display: "flex",
                                        flexWrap: "wrap",
                                        marginBottom: "0px",
                                      }}
                                    >
                                      {university.programmes
                                        .split("\n")
                                        .splice(0, 2)
                                        .map((course) => (
                                          <li
                                            className="px-2 py-1"
                                            style={{
                                              backgroundColor:
                                                "rgb(240, 248, 255)",
                                              fontSize: "12px",
                                              borderRadius: "0.25rem",
                                              margin: "0 8px 8px 0",
                                              width: "fit-content",
                                              listStyle: "none",
                                            }}
                                            key={generate()}
                                          >
                                            {coursesLinks.filter((cl) =>
                                              course.includes(cl.name)
                                            ).length > 0 ? (
                                              <Link
                                                href={
                                                  coursesLinks.filter((cl) =>
                                                    course.includes(cl.name)
                                                  )[0].link
                                                }
                                                passHref
                                              >
                                                <a
                                                  target="_blank"
                                                  style={{
                                                    textDecoration: "none",
                                                    color: "black",
                                                  }}
                                                >
                                                  {course}
                                                </a>
                                              </Link>
                                            ) : (
                                              <>{course}</>
                                            )}
                                          </li>
                                        ))}
                                    </ul>
                                    {university.programmes.split("\n").length >
                                    2 ? (
                                      <p className="text-end">
                                        {" "}
                                        <span
                                          className="text-primary px-3 py-2 rounded d-inline-block mt-3"
                                          onClick={() =>
                                            setupModal("course", university)
                                          }
                                          style={{
                                            fontSize: "14px",

                                            cursor: "pointer",
                                          }}
                                        >
                                          View More{" "}
                                          <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="16"
                                            height="16"
                                            fill="currentColor"
                                            className="bi bi-arrow-right"
                                            viewBox="0 0 16 16"
                                          >
                                            <path
                                              fillRule="evenodd"
                                              d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"
                                            />
                                          </svg>
                                        </span>
                                      </p>
                                    ) : null}
                                  </div>
                                </>
                              </div>
                            </Col>
                          ))}
                      </>
                    </Row>
                  )}
                  <Row>
                    {pageCount > 1 ? (
                      <Row className="mt-5" style={{ paddingBottom: "15px" }}>
                        <Col>
                          <ReactPaginate
                            previousLabel={<FaChevronLeft />}
                            nextLabel={<FaChevronRight />}
                            pageCount={pageCount}
                            onPageChange={changePage}
                            containerClassName={"paginationBtns"}
                            previousLinkClassName={"previousBtn"}
                            nextLinkClassName={"nextBtn"}
                            disabledClassName={"paginationDisabled"}
                            activeClassName={"paginationActive"}
                          />
                        </Col>
                      </Row>
                    ) : null}
                    <ReportIssueComponent
                      openModal={() => setupModal("report-issue")}
                    />
                  </Row>
                </Col>
              </Row>
            </TabPanel>
            <TabPanel header={<TabDistance />}>
              <Row>
                <Col md={3} className="d-none d-lg-block position-relative">
                  <p
                    className="fw-bold d-flex align-items-center"
                    style={{ fontSize: "18px", gap: "6px" }}
                  >
                    <BsFilter />
                    Distance Filters
                  </p>
                  <div
                    className="bg-white mb-3 shadow-2 p-4"
                    style={{
                      position: "relative",
                      padding: "8px",
                      borderRadius: "5px",
                    }}
                  >
                    {showAllStates ? (
                      <div className="categoery_modal">
                        <div className="position-relative h-100">
                          <div className="px-3 border-bottom">
                            <Row className="align-items-center">
                              <Col className="text-end">
                                <span
                                  onClick={() => setShowAllStates(false)}
                                  className="categ_modal_close"
                                >
                                  &times;
                                </span>
                              </Col>
                            </Row>
                          </div>
                          <div className="cate_modal_body">
                            {stateCopy
                              .slice(8, stateCopy.length)
                              .map((state) => (
                                <div key={state.id}>
                                  <label>
                                    <Form.Check
                                      name="state-check"
                                      defaultChecked={selectedStatesDistance.includes(
                                        state.id.toString()
                                      )}
                                      value={state.id}
                                      onChange={(e) => {
                                        if (e.target.checked) {
                                          setSelectedStatesDistance(
                                            (states) => [
                                              ...states,
                                              e.target.value,
                                            ]
                                          );
                                        } else {
                                          setSelectedStatesDistance(
                                            selectedStatesDistance.filter(
                                              (state) =>
                                                state !== e.target.value
                                            )
                                          );
                                        }
                                      }}
                                    />
                                    {state.state}
                                  </label>
                                </div>
                              ))}
                          </div>
                        </div>
                      </div>
                    ) : null}
                    <div className="d-flex justify-content-between">
                      {filterStates ? (
                        <div className="w-100 mb-1">
                          <InputGroup>
                            <FormControl
                              ref={stateTextRef}
                              className="shadow-none"
                              onChange={(e) =>
                                handleStateTextDistance(e.target.value)
                              }
                            />
                            <InputGroup.Text
                              onClick={() => {
                                setFilterStates(false);
                                setStateCopy(states);
                              }}
                              style={{ cursor: "pointer" }}
                            >
                              <FaTimes color="white" />{" "}
                            </InputGroup.Text>
                          </InputGroup>
                        </div>
                      ) : (
                        <>
                          <p
                            className="fw-bold d-flex align-items-center"
                            style={{ gap: "6px" }}
                          >
                            <FcAlphabeticalSortingZa className="d-none" />
                            States
                          </p>
                          <div
                            onClick={() => setFilterStates(true)}
                            className="d-flex justify-content-center align-items-center"
                            style={{
                              borderRadius: "50%",
                              height: "30px",
                              width: "30px",
                              backgroundColor: "#f5f5f6",
                              cursor: "pointer",
                            }}
                          >
                            <BsSearch />
                          </div>
                        </>
                      )}
                    </div>
                    <div style={{ maxHeight: "200px", overflowY: "scroll" }}>
                      {stateCopy.map((state) => (
                        <span key={state.id}>
                          <Form.Check
                            name="state-check"
                            defaultChecked={selectedStatesDistance.includes(
                              state.id.toString()
                            )}
                            label={state.state}
                            value={state.id}
                            onChange={(e) => {
                              if (e.target.checked) {
                                setSelectedStatesDistance((states) => [
                                  ...states,
                                  e.target.value,
                                ]);
                              } else {
                                setSelectedStatesDistance(
                                  selectedStatesDistance.filter(
                                    (state) => state !== e.target.value
                                  )
                                );
                              }
                            }}
                          />
                        </span>
                      ))}
                      {/* <div className='mt-1 mb-1 text-left ps-4'>
                    <span onClick={() => setShowAllStates(true)} style={{ fontSize: "14px", color: "#0074d7", cursor: "pointer" }}>View All</span>
                  </div> */}
                    </div>
                  </div>

                  <div
                    className="bg-white mb-3 shadow-2 p-4"
                    style={{
                      position: "relative",
                      padding: "8px",
                      borderRadius: "5px",
                    }}
                  >
                    <div className="d-flex justify-content-between">
                      <p
                        className="fw-bold d-flex align-items-center"
                        style={{ gap: "6px" }}
                      >
                        <FcGraduationCap className="d-none" /> Types
                      </p>
                      <FcInfo
                        onClick={() => setupModal("type-info")}
                        fontSize={18}
                        style={{ cursor: "pointer" }}
                      />
                    </div>
                    <div>
                      {types.map((type) => (
                        <span key={type.type}>
                          <Form.Check
                            name="type-check-distance"
                            label={type.type}
                            value={type.type}
                            defaultChecked={selectedTypesDistance.includes(
                              type.type.toString()
                            )}
                            onChange={(e) => {
                              if (e.target.checked) {
                                setSelectedTypesDistance((types) => [
                                  ...types,
                                  e.target.value,
                                ]);
                              } else {
                                setSelectedTypesDistance(
                                  selectedTypesDistance.filter(
                                    (type) => type !== e.target.value
                                  )
                                );
                              }
                            }}
                          />
                        </span>
                      ))}
                    </div>
                  </div>
                  <div
                    className="bg-white mb-3 shadow-2 p-4"
                    style={{
                      position: "relative",
                      padding: "8px",
                      borderRadius: "5px",
                    }}
                  >
                    <div className="d-flex justify-content-between">
                      <p
                        className="fw-bold d-flex align-items-center mb-3"
                        style={{ gap: "8px" }}
                      >
                        <FcLandscape className="d-none" />
                        Zones
                      </p>
                    </div>
                    <div>
                      {zones.map((zone) => (
                        <span key={zone}>
                          <Form.Check
                            name="zone-check-distance"
                            label={zone}
                            value={zone}
                            defaultChecked={selectedZonesDistance.includes(
                              zone.toString()
                            )}
                            onChange={(e) => {
                              if (e.target.checked) {
                                setSelectedZonesDistance((zones) => [
                                  ...zones,
                                  e.target.value,
                                ]);
                              } else {
                                setSelectedZonesDistance(
                                  selectedZonesDistance.filter(
                                    (zones) => zones !== e.target.value
                                  )
                                );
                              }
                            }}
                          />
                        </span>
                      ))}
                    </div>
                  </div>
                </Col>
                <Col lg={9}>
                  <Row className="justify-content-end mb-2">
                    <Col lg={12} style={{ zIndex: 999 }}>
                      <ReactSearchAutocomplete
                        onSearch={handleOnSearchDistance}
                        onClear={() => getAllUnivsDistance()}
                        placeholder="Is your Distance University Valid?"
                        onSelect={handleOnSelectDistance}
                        items={globalStore.universitiesDistance
                          .attach(Downgraded)
                          .get()}
                        formatResult={(item) => {
                          return (
                            <>
                              <span
                                style={{
                                  display: "flex",
                                  textAlign: "left",
                                  alignItems: "center",
                                  gap: "6px",
                                }}
                              >
                                {item.name}
                                <Badge
                                  style={{ fontSize: "12px" }}
                                  bg={
                                    item.mode === "ONLINE"
                                      ? "primary"
                                      : "warning"
                                  }
                                >
                                  {item.mode}
                                </Badge>
                              </span>
                            </>
                          );
                        }}
                      />
                      <span
                        className="ps-3 mt-3 text-success fw-bold d-block"
                        style={{ fontSize: "12px" }}
                      >
                        About{" "}
                        {
                          globalStore.universitiesDistanceCopy
                            .attach(Downgraded)
                            .get().length
                        }{" "}
                        results ({reqTime} seconds)
                      </span>
                    </Col>
                  </Row>
                  {searching ? (
                    <div
                      className="d-flex justify-content-center align-items-center"
                      style={{ height: "300px", color: "#0074d7" }}
                    >
                      <Spinner animation="border" size="lg" />
                    </div>
                  ) : globalStore.universitiesDistanceCopy.length === 0 ? (
                    <div
                      style={{
                        borderRadius: "8px",
                        padding: "16px",
                        backgroundColor: "white",
                        flexDirection: "column",
                      }}
                      className="d-flex align-items-center justify-content-center mb-5"
                    >
                      <h4>No results found!</h4>
                      <p className="text-center" style={{ fontSize: "14px" }}>
                        The university you are looking for may not be approved
                        by UGC for distance education. <br /> Contact our
                        experts for more details
                      </p>
                      <Link href="https://collegevidya.com/suggest-me-an-university">
                        <a target="_blank" className="suggest-me-btn">
                          <Image
                            src="/images/badge.png"
                            width={20}
                            height={20}
                            alt="badge"
                          />
                          Suggest Me Approved University in 2 mins
                        </a>
                      </Link>
                      <ReportIssueComponent
                        openModal={() => setupModal("report-issue")}
                      />
                    </div>
                  ) : (
                    <Row className="mt-3 row-cols-1 row-cols-md-2 row-cols-lg-2 row-cols-xl-3">
                      {globalStore.universitiesDistanceCopy
                        .attach(Downgraded)
                        .get()
                        .slice(
                          pagesVisitedDistance,
                          pagesVisitedDistance + universitiesPerPageDistance
                        )
                        .map((university) => (
                          <Col className="mb-4" key={university.id}>
                            <div
                              className="bg-white mb-3 h-100 shadow-2 overflow-hidden"
                              style={{ borderRadius: "16px" }}
                            >
                              <div
                                className="text-center d-flex justify-content-center align-items-start"
                                style={{
                                  borderRadius: "10px",
                                  backgroundColor: "#FFF",
                                }}
                              >
                                {university.image && university.image !== "" ? (
                                  <div
                                    className="univ-logo"
                                    style={{
                                      borderRadius: "8px",
                                      overflow: "hidden",
                                    }}
                                  >
                                    <Image
                                      src={university.image}
                                      width={306}
                                      height={222}
                                      alt="University Image"
                                      layout="responsive"
                                    />
                                  </div>
                                ) : (
                                  <Image
                                    src="/images/university_placeholder.png"
                                    height={150}
                                    width={150}
                                    alt="University Image"
                                  />
                                )}
                              </div>
                              <div className="px-3 pt-3">
                                <div className={`text-truncate`}>
                                  <Link
                                    href="https://collegevidya.com/suggest-me-an-university/"
                                    passHref
                                  >
                                    <a
                                      target="_blank"
                                      style={{
                                        gap: "6px",
                                        textDecoration: "none",
                                        color: "black",
                                      }}
                                    >
                                      <div style={{ fontSize: "16px" }}>
                                        <span style={{ fontWeight: "bold" }}>
                                          {university.name}
                                        </span>
                                      </div>
                                    </a>
                                  </Link>

                                  <span
                                    className="mt-2 rounded-pill bg-light text-dark fw-normal badge bgprimary"
                                    style={{ fontSize: "11px" }}
                                    role="button"
                                  >
                                    {university.type}
                                  </span>

                                  <div
                                    className="d-flex align-items-center mb-1 mt-2 py-2"
                                    style={{
                                      color: "#535766",
                                      gap: "6px",
                                      borderBottom: "1px solid #eee",
                                    }}
                                  >
                                    <FcLibrary fontSize={18} />
                                    <div>
                                      Mode of exam :{" "}
                                      <span className="fw-bold text-success">
                                        {" "}
                                        University Campus
                                      </span>
                                    </div>
                                  </div>
                                  <div
                                    className="d-flex align-items-center mb-1 mt-2 py-2"
                                    style={{
                                      color: "#535766",
                                      gap: "6px",
                                      borderBottom: "1px solid #eee",
                                    }}
                                  >
                                    <FcReading fontSize={18} />
                                    <div>
                                      Mode of education :{" "}
                                      <span className="fw-bold text-success">
                                        {" "}
                                        Online
                                      </span>
                                    </div>
                                  </div>
                                  <div
                                    className="d-flex align-items-center mb-1 py-2 text-lowercase"
                                    style={{
                                      color: "#535766",
                                      gap: "6px",
                                      borderBottom: "1px solid #eee",
                                    }}
                                  >
                                    <FcGlobe fontSize={18} />
                                    <div>
                                      {university.state} ({university.zone})
                                    </div>
                                  </div>
                                  <div
                                    className="mb-1 d-flex align-items-center py-2"
                                    style={{
                                      fontSize: "14px",
                                      color: "#535766",
                                      gap: "6px",
                                    }}
                                  >
                                    <FcRules fontSize={18} />{" "}
                                    <span>
                                      Programmes:{" "}
                                      <b>{university.number_of_programmes}</b>
                                    </span>
                                  </div>
                                </div>
                                <div
                                  className="d-flex align-items-center"
                                  style={{ gap: "6px" }}
                                ></div>

                                <p className="text-end">
                                  {" "}
                                  {university.programmes.split("\n") ? (
                                    <span
                                      className="text-primary"
                                      onClick={() =>
                                        setupModal("course", university)
                                      }
                                      style={{
                                        fontSize: "14px",

                                        cursor: "pointer",
                                      }}
                                    >
                                      View More{" "}
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="16"
                                        height="16"
                                        fill="currentColor"
                                        className="bi bi-arrow-right"
                                        viewBox="0 0 16 16"
                                      >
                                        <path
                                          fillRule="evenodd"
                                          d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"
                                        />
                                      </svg>
                                    </span>
                                  ) : null}
                                </p>
                              </div>
                            </div>
                          </Col>
                        ))}
                    </Row>
                  )}
                  <Row>
                    {" "}
                    {pageCountDistance > 1 ? (
                      <Row className="mt-5" style={{ paddingBottom: "15px" }}>
                        <Col>
                          <ReactPaginate
                            previousLabel={<FaChevronLeft />}
                            nextLabel={<FaChevronRight />}
                            pageCount={pageCountDistance}
                            onPageChange={changePageDistance}
                            containerClassName={"paginationBtns"}
                            previousLinkClassName={"previousBtn"}
                            nextLinkClassName={"nextBtn"}
                            disabledClassName={"paginationDisabled"}
                            activeClassName={"paginationActive"}
                          />
                        </Col>
                      </Row>
                    ) : null}
                    <ReportIssueComponent
                      openModal={() => setupModal("report-issue")}
                    />
                  </Row>
                </Col>
              </Row>
            </TabPanel>
            <TabPanel header={<TabWes />}>
              <Container>
                <Row>
                  <Col lg={12}>
                    <Col lg={12} style={{ zIndex: 999 }}>
                      <ReactSearchAutocomplete
                        onSearch={handleOnSearchWES}
                        onClear={() => getAllWES()}
                        placeholder="Is your University WES approved?"
                        onSelect={handleOnSelectWES}
                        items={globalStore.wes.attach(Downgraded).get()}
                      />
                      <span
                        className="ps-3 mt-2 text-success fw-bold mb-3 d-block"
                        style={{ fontSize: "12px" }}
                      >
                        About{" "}
                        {globalStore.wesCopy.attach(Downgraded).get().length}{" "}
                        results ({reqTime} seconds)
                      </span>
                    </Col>
                    <Row className="row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4">
                      {searching ? (
                        <div
                          className="d-flex justify-content-center align-items-center"
                          style={{ height: "300px", color: "#0074d7" }}
                        >
                          <Spinner animation="border" size="lg" />
                        </div>
                      ) : globalStore.wesCopy.length === 0 ? (
                        <div
                          style={{
                            borderRadius: "8px",
                            padding: "16px",
                            backgroundColor: "white",
                            flexDirection: "column",
                          }}
                          className="d-flex align-items-center justify-content-center mb-5"
                        >
                          <h4>No results found!</h4>
                          <p
                            className="text-center"
                            style={{ fontSize: "14px" }}
                          >
                            The University you are looking for may not be
                            approved by UGC. <br /> Contact our experts for more
                            details
                          </p>
                          <Link href="https://collegevidya.com/suggest-me-an-university">
                            <a target="_blank" className="suggest-me-btn">
                              <Image
                                src="/images/badge.png"
                                width={20}
                                height={20}
                                alt="badge"
                              />
                              Suggest Me Approved University in 2 mins
                            </a>
                          </Link>
                          <ReportIssueComponent
                            openModal={() => setupModal("report-issue")}
                          />
                        </div>
                      ) : (
                        globalStore.wesCopy
                          .attach(Downgraded)
                          .get()
                          .slice(pagesVisitedWES, pagesVisitedWES + WESPerPage)
                          .map((wUniv) => (
                            <Col className="mb-4" key={wUniv.id}>
                              <WesSearchCard
                                name={wUniv.name}
                                image={wUniv.image}
                              />
                            </Col>
                          ))
                      )}
                    </Row>
                    {pageCountWES > 1 ? (
                      <Row className="mt-5" style={{ paddingBottom: "15px" }}>
                        <Col>
                          <ReactPaginate
                            previousLabel={<FaChevronLeft />}
                            nextLabel={<FaChevronRight />}
                            pageCount={pageCountWES}
                            onPageChange={changePageWES}
                            containerClassName={"paginationBtns"}
                            previousLinkClassName={"previousBtn"}
                            nextLinkClassName={"nextBtn"}
                            disabledClassName={"paginationDisabled"}
                            activeClassName={"paginationActive"}
                          />
                        </Col>
                      </Row>
                    ) : null}
                    <ReportIssueComponent
                      openModal={() => setupModal("report-issue")}
                    />
                  </Col>
                </Row>
              </Container>
            </TabPanel>
            <TabPanel header={<TabBoard />}>
              <Container>
                <Row>
                  <Col className="col-12 col-lg-3 d-none d-lg-block position-relative">
                    <p
                      className="fw-bold d-flex align-items-center"
                      style={{ fontSize: "18px", gap: "6px" }}
                    >
                      <BsFilter /> Filters
                    </p>

                    <div
                      className="bg-white mb-3 shadow-2 p-4"
                      style={{
                        position: "relative",
                        padding: "8px",
                        borderRadius: "5px",
                      }}
                    >
                      {showAllStates ? (
                        <div className="categoery_modal">
                          <div className="position-relative h-100">
                            <div className="px-3 border-bottom">
                              <Row className="align-items-center">
                                <Col className="text-end">
                                  <span
                                    onClick={() => setShowAllStates(false)}
                                    className="categ_modal_close"
                                  >
                                    &times;
                                  </span>
                                </Col>
                              </Row>
                            </div>
                            <div className="cate_modal_body">
                              {stateCopy
                                .slice(8, stateCopy.length)
                                .map((state) => (
                                  <div key={state.id}>
                                    <label>
                                      <Form.Check
                                        name="state-check"
                                        defaultChecked={selectedStates.includes(
                                          state.id.toString()
                                        )}
                                        value={state.id}
                                        onChange={(e) => {
                                          if (e.target.checked) {
                                            setSelectedStates((states) => [
                                              ...states,
                                              e.target.value,
                                            ]);
                                          } else {
                                            setSelectedStates(
                                              selectedStates.filter(
                                                (state) =>
                                                  state !== e.target.value
                                              )
                                            );
                                          }
                                        }}
                                      />
                                      {state.state}
                                    </label>
                                  </div>
                                ))}
                            </div>
                          </div>
                        </div>
                      ) : null}
                      <div className="d-flex justify-content-between">
                        {filterStates ? (
                          <div className="w-100 mb-1">
                            <InputGroup>
                              <FormControl
                                ref={stateTextRef}
                                className="shadow-none"
                                onChange={(e) =>
                                  handleStateTextBoard(e.target.value)
                                }
                              />
                              <InputGroup.Text
                                onClick={() => {
                                  setFilterStates(false);
                                  setStateCopy(states);
                                }}
                                style={{ cursor: "pointer" }}
                              >
                                <FaTimes color="white" />{" "}
                              </InputGroup.Text>
                            </InputGroup>
                          </div>
                        ) : (
                          <>
                            <p
                              className="fw-bold d-flex align-items-center"
                              style={{ gap: "6px" }}
                            >
                              <FcAlphabeticalSortingZa className="d-none" />
                              States
                            </p>
                            <div
                              onClick={() => setFilterStates(true)}
                              className="d-flex justify-content-center align-items-center"
                              style={{
                                borderRadius: "50%",
                                height: "30px",
                                width: "30px",
                                backgroundColor: "#f5f5f6",
                                cursor: "pointer",
                              }}
                            >
                              <BsSearch />
                            </div>
                          </>
                        )}
                      </div>
                      <div style={{ maxHeight: "300px", overflowY: "scroll" }}>
                        {stateCopyBoard.map((state) => (
                          <span key={state.id}>
                            <Form.Check
                              name="state-check"
                              defaultChecked={selectedStatesBoard.includes(
                                state.id.toString()
                              )}
                              label={state.state}
                              value={state.id}
                              onChange={(e) => {
                                if (e.target.checked) {
                                  setSelectedStatesBoard((states) => [
                                    ...states,
                                    e.target.value,
                                  ]);
                                } else {
                                  setSelectedStatesBoard(
                                    selectedStatesBoard.filter(
                                      (state) => state !== e.target.value
                                    )
                                  );
                                }
                              }}
                            />
                          </span>
                        ))}
                      </div>
                    </div>
                  </Col>
                  <Col className="col-12 col-lg-9">
                    <Col lg={12} style={{ zIndex: 999 }}>
                      <ReactSearchAutocomplete
                        onSearch={handleOnSearchBoards}
                        onClear={() => getAllBoards()}
                        placeholder="Is your School Board Valid?"
                        onSelect={handleOnSelectBoard}
                        items={globalStore.boards.attach(Downgraded).get()}
                      />
                      <span
                        className="ps-3 mt-2 text-success fw-bold mb-3 d-block"
                        style={{ fontSize: "12px" }}
                      >
                        About{" "}
                        {globalStore.boardsCopy.attach(Downgraded).get().length}{" "}
                        results ({reqTime} seconds)
                      </span>
                    </Col>
                    <Row className="row-cols-1 row-cols-sm-2 row-cols-xl-3">
                      {searching ? (
                        <div
                          className="d-flex justify-content-center align-items-center"
                          style={{ height: "300px", color: "#0074d7" }}
                        >
                          <Spinner animation="border" size="lg" />
                        </div>
                      ) : globalStore.boardsCopy.length === 0 ? (
                        <div
                          style={{
                            borderRadius: "8px",
                            padding: "16px",
                            backgroundColor: "white",
                            flexDirection: "column",
                          }}
                          className="d-flex align-items-center justify-content-center mb-5"
                        >
                          <h4>No results found!</h4>
                          <p
                            className="text-center"
                            style={{ fontSize: "14px" }}
                          >
                            The school board you are looking for may not be
                            approved by UGC. <br /> Contact our experts for more
                            details
                          </p>
                          <Link href="https://collegevidya.com/suggest-me-an-university">
                            <a target="_blank" className="suggest-me-btn">
                              <Image
                                src="/images/badge.png"
                                width={20}
                                height={20}
                                alt="badge"
                              />
                              Suggest Me Approved University in 2 mins
                            </a>
                          </Link>
                          <ReportIssueComponent
                            openModal={() => setupModal("report-issue")}
                          />
                        </div>
                      ) : (
                        globalStore.boardsCopy
                          .attach(Downgraded)
                          .get()
                          .slice(
                            pagesVisitedBoard,
                            pagesVisitedBoard + boardsPerPage
                          )
                          .map((board) => (
                            <Col className="mb-4" key={board.id}>
                              <SearchCard
                                image={board.image}
                                name={board.name}
                                code={board.code}
                                state={board.state}
                              />
                            </Col>
                          ))
                      )}
                    </Row>
                    {pageCountBoard > 1 ? (
                      <Row className="mt-5" style={{ paddingBottom: "15px" }}>
                        <Col>
                          <ReactPaginate
                            previousLabel={<FaChevronLeft />}
                            nextLabel={<FaChevronRight />}
                            pageCount={pageCountBoard}
                            onPageChange={changePageBoard}
                            containerClassName={"paginationBtns"}
                            previousLinkClassName={"previousBtn"}
                            nextLinkClassName={"nextBtn"}
                            disabledClassName={"paginationDisabled"}
                            activeClassName={"paginationActive"}
                          />
                        </Col>
                      </Row>
                    ) : null}
                    <ReportIssueComponent
                      openModal={() => setupModal("report-issue")}
                    />
                  </Col>
                </Row>
              </Container>
            </TabPanel>
            <TabPanel header={<TabRegular />}>
              <Container>
                <Row>
                  <Col className="col-12 col-lg-3 d-none d-lg-block position-relative">
                    <p
                      className="fw-bold d-flex align-items-center"
                      style={{ fontSize: "18px", gap: "6px" }}
                    >
                      <BsFilter /> Regular Filters
                    </p>

                    <div
                      className="bg-white mb-3 shadow-2 p-4"
                      style={{
                        position: "relative",
                        padding: "8px",
                        borderRadius: "5px",
                      }}
                    >
                      {showAllStates ? (
                        <div className="categoery_modal">
                          <div className="position-relative h-100">
                            <div className="px-3 border-bottom">
                              <Row className="align-items-center">
                                <Col className="text-end">
                                  <span
                                    onClick={() => setShowAllStates(false)}
                                    className="categ_modal_close"
                                  >
                                    &times;
                                  </span>
                                </Col>
                              </Row>
                            </div>
                            <div className="cate_modal_body">
                              {stateCopyRegular
                                .slice(8, stateCopyRegular.length)
                                .map((state) => (
                                  <div key={state.id}>
                                    <label>
                                      <Form.Check
                                        name="state-check"
                                        defaultChecked={selectedStatesRegular.includes(
                                          state.id.toString()
                                        )}
                                        value={state.id}
                                        onChange={(e) => {
                                          if (e.target.checked) {
                                            setSelectedStatesRegular(
                                              (states) => [
                                                ...states,
                                                e.target.value,
                                              ]
                                            );
                                          } else {
                                            setSelectedStatesRegular(
                                              selectedStatesRegular.filter(
                                                (state) =>
                                                  state !== e.target.value
                                              )
                                            );
                                          }
                                        }}
                                      />
                                      {state.state}
                                    </label>
                                  </div>
                                ))}
                            </div>
                          </div>
                        </div>
                      ) : null}
                      <div className="d-flex justify-content-between">
                        {filterStates ? (
                          <div className="w-100 mb-1">
                            <InputGroup>
                              <FormControl
                                ref={stateTextRef}
                                className="shadow-none"
                                onChange={(e) =>
                                  handleStateTextRegular(e.target.value)
                                }
                              />
                              <InputGroup.Text
                                onClick={() => {
                                  setFilterStates(false);
                                  setStateCopyRegular(statesRegular);
                                }}
                                style={{ cursor: "pointer" }}
                              >
                                <FaTimes color="white" />{" "}
                              </InputGroup.Text>
                            </InputGroup>
                          </div>
                        ) : (
                          <>
                            <p
                              className="fw-bold d-flex align-items-center"
                              style={{ gap: "6px" }}
                            >
                              <FcAlphabeticalSortingZa className="d-none" />
                              States
                            </p>
                            <div
                              onClick={() => setFilterStates(true)}
                              className="d-flex justify-content-center align-items-center"
                              style={{
                                borderRadius: "50%",
                                height: "30px",
                                width: "30px",
                                backgroundColor: "#f5f5f6",
                                cursor: "pointer",
                              }}
                            >
                              <BsSearch />
                            </div>
                          </>
                        )}
                      </div>
                      <div style={{ maxHeight: "200px", overflowY: "scroll" }}>
                        {stateCopyRegular.map((state) => (
                          <span key={state.id}>
                            <Form.Check
                              name="state-check"
                              defaultChecked={selectedStatesRegular.includes(
                                state.id.toString()
                              )}
                              label={state.state}
                              value={state.id}
                              onChange={(e) => {
                                if (e.target.checked) {
                                  setSelectedStatesRegular((states) => [
                                    ...states,
                                    e.target.value,
                                  ]);
                                } else {
                                  setSelectedStatesRegular(
                                    selectedStatesRegular.filter(
                                      (state) => state !== e.target.value
                                    )
                                  );
                                }
                              }}
                            />
                          </span>
                        ))}
                      </div>
                    </div>

                    <div
                      className="bg-white mb-3 shadow-2 p-4"
                      style={{
                        position: "relative",
                        padding: "8px",
                        borderRadius: "5px",
                      }}
                    >
                      <div className="d-flex justify-content-between">
                        <p
                          className="fw-bold d-flex align-items-center"
                          style={{ gap: "6px" }}
                        >
                          <FcGraduationCap className="d-none" /> Types
                        </p>
                        <FcInfo
                          onClick={() => setupModal("type-info")}
                          fontSize={18}
                          style={{ cursor: "pointer" }}
                        />
                      </div>
                      <div>
                        {typesRegular.map((type) => (
                          <span key={type.type}>
                            <Form.Check
                              name="type-check"
                              label={type.type.toUpperCase()}
                              value={type.type}
                              onChange={(e) => {
                                if (e.target.checked) {
                                  setSelectedTypesRegular((types) => [
                                    ...types,
                                    e.target.value,
                                  ]);
                                } else {
                                  setSelectedTypesRegular(
                                    selectedTypesRegular.filter(
                                      (type) => type !== e.target.value
                                    )
                                  );
                                }
                              }}
                            />
                          </span>
                        ))}
                      </div>
                    </div>
                  </Col>
                  <Col className="col-12 col-lg-9">
                    <Col lg={12} style={{ zIndex: 999 }}>
                      <ReactSearchAutocomplete
                        onSearch={handleOnSearchRegular}
                        onClear={() => getAllRegular()}
                        placeholder="Is your Regular University Valid?"
                        onSelect={handleOnSelectRegular}
                        items={globalStore.regulars.attach(Downgraded).get()}
                      />
                      <span
                        className="ps-3 mt-2 text-success fw-bold mb-3 d-block"
                        style={{ fontSize: "12px" }}
                      >
                        About{" "}
                        {
                          globalStore.regularsCopy.attach(Downgraded).get()
                            .length
                        }{" "}
                        results ({reqTime} seconds)
                      </span>
                    </Col>
                    <Row className="row-cols-1 row-cols-sm-2 row-cols-xl-3">
                      {searching ? (
                        <div
                          className="d-flex justify-content-center align-items-center"
                          style={{ height: "300px", color: "#0074d7" }}
                        >
                          <Spinner animation="border" size="lg" />
                        </div>
                      ) : globalStore.regularsCopy.length === 0 ? (
                        <div
                          style={{
                            borderRadius: "8px",
                            padding: "16px",
                            backgroundColor: "white",
                            flexDirection: "column",
                          }}
                          className="d-flex align-items-center justify-content-center mb-5"
                        >
                          <h4>No results found!</h4>
                          <p
                            className="text-center"
                            style={{ fontSize: "14px" }}
                          >
                            The University you are looking for may not be
                            approved by UGC. <br /> Contact our experts for more
                            details
                          </p>
                          <Link href="https://collegevidya.com/suggest-me-an-university">
                            <a target="_blank" className="suggest-me-btn">
                              <Image
                                src="/images/badge.png"
                                width={20}
                                height={20}
                                alt="badge"
                              />
                              Suggest Me Approved University in 2 mins
                            </a>
                          </Link>
                          <ReportIssueComponent
                            openModal={() => setupModal("report-issue")}
                          />
                        </div>
                      ) : (
                        globalStore.regularsCopy
                          .attach(Downgraded)
                          .get()
                          .slice(
                            pagesVisitedRegular,
                            pagesVisitedRegular + regularsPerPage
                          )
                          .map((regular) => (
                            <Col className="mb-4" key={regular.id}>
                              <RegularSearchCard
                                name={regular.name}
                                image={regular.image}
                                type={regular.type}
                                state={regular.state}
                                year={regular.year}
                                bgColor={
                                  badgeColorRegular.filter(
                                    (color) => color.tag === regular.type
                                  )[0].variant
                                }
                                showInfo={() => setupModal("type-info")}
                              />
                            </Col>
                          ))
                      )}
                    </Row>
                    {pageCountRegular > 1 ? (
                      <Row className="mt-5" style={{ paddingBottom: "15px" }}>
                        <Col>
                          <ReactPaginate
                            previousLabel={<FaChevronLeft />}
                            nextLabel={<FaChevronRight />}
                            pageCount={pageCountRegular}
                            onPageChange={changePageRegular}
                            containerClassName={"paginationBtns"}
                            previousLinkClassName={"previousBtn"}
                            nextLinkClassName={"nextBtn"}
                            disabledClassName={"paginationDisabled"}
                            activeClassName={"paginationActive"}
                          />
                        </Col>
                      </Row>
                    ) : null}
                    <ReportIssueComponent
                      openModal={() => setupModal("report-issue")}
                    />
                  </Col>
                </Row>
              </Container>
            </TabPanel>
            <TabPanel header={<TabFake />}>
              <Container>
                <Row>
                  <Col lg={12}>
                    <Col lg={12} style={{ zIndex: 999 }}>
                      <ReactSearchAutocomplete
                        onSearch={handleOnSearchFake}
                        onClear={() => getAllFake()}
                        placeholder="Is your University WES approved?"
                        onSelect={handleOnSelectFake}
                        items={globalStore.fake.attach(Downgraded).get()}
                      />
                      <span
                        className="ps-3 mt-2 text-success fw-bold mb-3 d-block"
                        style={{ fontSize: "12px" }}
                      >
                        About{" "}
                        {globalStore.fakeCopy.attach(Downgraded).get().length}{" "}
                        results ({reqTime} seconds)
                      </span>
                    </Col>
                    <Row className="row-cols-1 row-cols-sm-2 row-cols-lg-3  row-cols-xl-4">
                      {searching ? (
                        <div
                          className="d-flex justify-content-center align-items-center"
                          style={{ height: "300px", color: "#0074d7" }}
                        >
                          <Spinner animation="border" size="lg" />
                        </div>
                      ) : globalStore.fakeCopy.length === 0 ? (
                        <div
                          style={{
                            borderRadius: "8px",
                            padding: "16px",
                            backgroundColor: "white",
                            flexDirection: "column",
                          }}
                          className="d-flex align-items-center justify-content-center mb-5"
                        >
                          <h4>No results found!</h4>
                          <p
                            className="text-center"
                            style={{ fontSize: "14px" }}
                          >
                            The University you are looking for may be approved
                            by UGC. <br /> Contact our experts for more details
                          </p>
                          <Link href="https://collegevidya.com/suggest-me-an-university">
                            <a target="_blank" className="suggest-me-btn">
                              <Image
                                src="/images/badge.png"
                                width={20}
                                height={20}
                                alt="badge"
                              />
                              Suggest Me Approved University in 2 mins
                            </a>
                          </Link>
                          <ReportIssueComponent
                            openModal={() => setupModal("report-issue")}
                          />
                        </div>
                      ) : (
                        globalStore.fakeCopy
                          .attach(Downgraded)
                          .get()
                          .slice(
                            pagesVisitedFake,
                            pagesVisitedFake + FakePerPage
                          )
                          .map((fUniv) => (
                            <Col className="mb-4" key={fUniv.id}>
                              <WesSearchCard
                                name={fUniv.name}
                                image={fUniv.image}
                              />
                            </Col>
                          ))
                      )}
                    </Row>
                    {pageCountFake > 1 ? (
                      <Row className="mt-5" style={{ paddingBottom: "15px" }}>
                        <Col>
                          <ReactPaginate
                            previousLabel={<FaChevronLeft />}
                            nextLabel={<FaChevronRight />}
                            pageCount={pageCountFake}
                            onPageChange={changePageFake}
                            containerClassName={"paginationBtns"}
                            previousLinkClassName={"previousBtn"}
                            nextLinkClassName={"nextBtn"}
                            disabledClassName={"paginationDisabled"}
                            activeClassName={"paginationActive"}
                          />
                        </Col>
                      </Row>
                    ) : null}
                    <ReportIssueComponent
                      openModal={() => setupModal("report-issue")}
                    />
                  </Col>
                </Row>
              </Container>
            </TabPanel>
          </TabView>
        </Container>
      </section>

      <section className="py-5">
        <Container>
          <Row>
            <Col>
              <div className="py-4 py-md-5">
                <Container>
                  <Row>
                    <Col className="col-12 text-start">
                      <p className="fw-bold h4 textprimary">
                        Ask our team anything, anytime
                      </p>
                      <p className="fs-16 text-dark mb-3">
                        Our team of experts, or experienced individuals, will
                        answer it within 24 hours.
                      </p>
                      <Link href={"https://collegevidya.com/question-answer/"}>
                        <a
                          className="fw-bold fs-14 text-white rounded-pill px-3 py-2 bgprimary shadow-sm border border-primary text-nowrap"
                          target="_blank"
                        >
                          Ask any Question - CV Panel
                        </a>
                      </Link>
                    </Col>
                  </Row>
                </Container>
              </div>
            </Col>
          </Row>
          <OurExpert />
        </Container>
      </section>

      <section>
        <Container></Container>
      </section>
      <Footer menuData={menuListData} />
      <CoursesModal
        show={showCourseModal}
        onHide={() => setShowCourseModal(false)}
      />
      <OtherModal
        show={showOtherModal}
        onHide={() => setShowOtherModal(false)}
      />
      <SubmitModal
        show={showSubmitModal}
        onHide={() => setShowSubmitModal(false)}
      />
    </div>
  );
}
