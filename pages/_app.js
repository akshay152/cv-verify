import "bootstrap/dist/css/bootstrap.min.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.min.css";
import 'semantic-ui-css/semantic.min.css'
import React, { useEffect, useState } from "react";
import { SSRProvider } from "react-bootstrap";
import TagManager from "react-gtm-module";
import Header from "../components/global/Header/Header";
import StickyFooter from "../components/global/StickyFooter/StickyFooter";
import MobileFilter from "../components/mobile/Filter/MobileFilter";
import MobileFilterBoard from "../components/mobile/Filter/MobileFilterBoard";
import MobileFilterRegular from "../components/mobile/Filter/MobileFilterRegular";
import MobileFilterWes from "../components/mobile/Filter/MobileFilterWes";
import MobileFilterDistance from "../components/mobile/Filter/MobileFilterDistance";
import { Toaster } from "react-hot-toast";
import axios from "axios";
import { useHookstate } from "@hookstate/core";
import store from "../utils/store";
import "../styles/globals.css";


function MyApp({ Component, pageProps }) {
  const [showModal, setShowModal] = React.useState(false);
  const [showBoardModal, setShowBoardModal] = React.useState(false);
  const [showRegularModal, setShowRegularModal] = React.useState(false);
  const [showWesModal, setShowWesModal] = React.useState(false);
  const [showDistanceModal, setShowDistanceModal] = React.useState(false);
   const { tagLine, universalLogo } =
     useHookstate(store);

  useEffect(() => {
    TagManager.initialize({ gtmId: "GTM-TX88D2L" });
  }, []);

    useEffect(() => {
      axios
        .get("https://admin.collegevidya.com/website_logo/")
        .then((response) => {
          const primary_logo = response.data.primary_logo;
          const tag_line = response.data.tag_line;
          universalLogo.set(primary_logo);
          tagLine.set(tag_line);
        });
    }, []);



  return (
    <SSRProvider>
      <Toaster />
      <Header />
      <Component {...pageProps} />
      <MobileFilter showModal={showModal} setShowModal={setShowModal} />
      <MobileFilterBoard
        showBoardModal={showBoardModal}
        setShowBoardModal={setShowBoardModal}
      />
      <MobileFilterRegular
        showRegularModal={showRegularModal}
        setShowRegularModal={setShowRegularModal}
      />
      <MobileFilterWes
        showWesModal={showWesModal}
        setShowWesModal={setShowWesModal}
      />
      <MobileFilterDistance
        showDistanceModal={showDistanceModal}
        setShowDistanceModal={setShowDistanceModal}
      />

      <StickyFooter
        setShowModal={setShowModal}
        setShowBoardModal={setShowBoardModal}
        setShowRegularModal={setShowRegularModal}
        setShowWesModal={setShowWesModal}
        setShowDistanceModal={setShowDistanceModal}
      />
    </SSRProvider>
  );
}

export default MyApp;
