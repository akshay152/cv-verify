import axios from "axios";

export default function Misc() {
  return <></>;
}

export const getStates = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "states");
};

export const getStatesBoard = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "states/board");
};

export const getStatesRegular = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "states/regular");
};

export const getTypes = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "types");
};

export const getTypesRegular = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "types/regular");
};

export const getAllUniversities = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "universities/all");
};

export const getAllUniversitiesDistance = () => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + "universities/all/distance"
  );
};

export const getAllVerifiedBoards = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "boards/all");
};

export const getAllRegularUniversities = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "regulars/all");
};

export const getAllWESUniversities = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "wes/all");
};

export const getAllFakeUniversities = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + "fake/all");
};

export const getFilteredUniversities = (modes, states, types, zones) => {
  var formData = new FormData();
  formData.append("modes", JSON.stringify(modes));
  formData.append("states", JSON.stringify(states));
  formData.append("types", JSON.stringify(types));
  formData.append("zones", JSON.stringify(zones));
  return axios.post(
    process.env.NEXT_PUBLIC_BACKEND_URL + "universities/filtered",
    formData
  );
};

export const getFilteredUniversitiesDistance = (
  modes,
  states,
  types,
  zones
) => {
  var formData = new FormData();
  formData.append("modes", JSON.stringify(modes));
  formData.append("states", JSON.stringify(states));
  formData.append("types", JSON.stringify(types));
  formData.append("zones", JSON.stringify(zones));
  return axios.post(
    process.env.NEXT_PUBLIC_BACKEND_URL + "universities/filtered/distance",
    formData
  );
};

export const getFilteredBoards = (states) => {
  var formData = new FormData();
  formData.append("states", JSON.stringify(states));
  return axios.post(
    process.env.NEXT_PUBLIC_BACKEND_URL + "boards/filtered",
    formData
  );
};

export const getFilteredRegulars = (states, types) => {
  var formData = new FormData();
  formData.append("states", JSON.stringify(states));
  formData.append("types", JSON.stringify(types));
  return axios.post(
    process.env.NEXT_PUBLIC_BACKEND_URL + "regulars/filtered",
    formData
  );
};

export const filterByName = (name) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + "universities/name?name=" + name
  );
};

export const filterByNameDistance = (name) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL +
      "universities/name/distance?name=" +
      name
  );
};

export const filterByBoardName = (boardName) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + "boards/name?name=" + boardName
  );
};

export const filterByRegularName = (regularName) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + "regulars/name?name=" + regularName
  );
};

export const filterByWESName = (wesName) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + "wes/name?name=" + wesName
  );
};

export const filterByFakeName = (fakeName) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + "fake/name?name=" + fakeName
  );
};

export const submitIssue = (data) => {
  return axios.post(process.env.NEXT_PUBLIC_BACKEND_URL + "issue", data);
};
