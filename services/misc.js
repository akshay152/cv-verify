const axios = require("axios");

export const getMenu = () => {
  return axios.get(process.env.NEXT_PUBLIC_ADMIN_URL + "menu");
};

export const getHomePageUniversityData = () => {
  return axios.get(
    process.env.NEXT_PUBLIC_ADMIN_URL + "home_page_universities"
  );
};
