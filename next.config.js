/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      "collegevidya.s3.ap-south-1.amazonaws.com",
      "collegevidyanew.s3.amazonaws.com",
      "collegevidyanew.s3.ap-south-1.amazonaws.com",
      "cv-counsellors.s3.ap-south-1.amazonaws.com",
      "collegevidya.com",
      "d1aeya7jd2fyco.cloudfront.net",
      "d6wn6379hq47k.cloudfront.net",
      "universityadmission.co.in",
      "chats.collegevidya.com",
    ],
  },
};

module.exports = nextConfig;
