import { createState } from "@hookstate/core";

const store = createState({
  activeIndex: 0,
  universities: [],
  universitiesCopy: [],
  universitiesDistance: [],
  universitiesDistanceCopy: [],
  boards: [],
  boardsCopy: [],
  regulars: [],
  regularsCopy: [],
  wes: [],
  wesCopy: [],
  fake: [],
  fakeCopy: [],
  universalLogo:
    "https://d1aeya7jd2fyco.cloudfront.net/logo/universal-logo.png",
  tagLine: "#ChunoApnaSahi",
});

export default store;
