import React from "react";
import { Modal } from "react-bootstrap";
import { FcInfo } from "react-icons/fc";
import ModeInfoModal from "../../modals/ModeInfoModal";
import TypeInfoModal from "../../modals/TypeInfoModal";
import styles from "../Filter/MobileFilter.module.css";

const FilterNav = (props) => {
  const [showOtherModal, setShowOtherModal] = React.useState(false);
  const [otherModalContent, setOtherModalContent] = React.useState(null);
  const [modalSize, setCourseModalSize] = React.useState("md");

  function setupModal(key, ekey = "") {
    if (key === "mode-info") {
      setCourseModalSize("lg");
      setOtherModalContent(
        <ModeInfoModal closeModal={() => setShowOtherModal(false)} />
      );
      setShowOtherModal(true);
    }
    if (key === "type-info") {
      setCourseModalSize("md");
      setOtherModalContent(
        <TypeInfoModal closeModal={() => setShowOtherModal(false)} />
      );
      setShowOtherModal(true);
    }
  }

  function OtherModal(props) {
    return (
      <Modal {...props} size={modalSize} scrollable={true} centered>
        {otherModalContent}
      </Modal>
    );
  }

  return (
    <div
      onClick={() => props.onClick()}
      className={`${styles.filter_nav} ${
        props.active ? styles.active : ""
      } d-flex align-items-center`}
      style={{ gap: "6px" }}>
      {props.logo}
      <span style={{fontSize:"12px"}}>{props.title}</span>
      {props.title === "Modes" ? (
        <FcInfo
          onClick={() => setupModal("mode-info")}
          fontSize={18}
          style={{ cursor: "pointer" }}
        />
      ) : null}
      {props.title === "Types" ? (
        <FcInfo
          onClick={() => setupModal("type-info")}
          fontSize={18}
          style={{ cursor: "pointer" }}
        />
      ) : null}
      <OtherModal
        show={showOtherModal}
        onHide={() => setShowOtherModal(false)}
      />
    </div>
  );
};

export default FilterNav;
