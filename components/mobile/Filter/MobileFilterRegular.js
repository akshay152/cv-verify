import React, { useEffect } from "react";
import { useState } from "@hookstate/core";
import { Col, Modal, Row } from "react-bootstrap";

import StateFilter from "../FilterComponents/StateFilter";
import TypeFilter from "../FilterComponents/TypeFilter";
import FilterNav from "../FilterNav/FilterNav";
import store from "../../../utils/store";
import {
  getAllRegularUniversities,
  getFilteredRegulars,
  getStatesRegular,
  getTypesRegular,
} from "../../../pages/services/Misc";
import { FcGraduationCap, FcLandscape } from "react-icons/fc";

const MobileFilterRegular = (props) => {
  const [active, setActive] = React.useState("states");

  const [states, setStates] = React.useState([]);
  const [selectedStates, setSelectedStates] = React.useState([]);

  const [types, setTypes] = React.useState([]);
  const [selectedTypes, setSelectedTypes] = React.useState([]);

  const globalStore = useState(store);

  function getAllRegular() {
    getAllRegularUniversities().then((response) => {
      globalStore.regulars.set(response.data);
      globalStore.regularsCopy.set(response.data);
    });
  }

  useEffect(() => {
    getStatesRegular().then((response) => setStates(response.data));
    getTypesRegular().then((response) => setTypes(response.data));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (selectedStates.length === 0 && selectedTypes.length === 0) {
      getAllRegular();
    } else {
      getFilteredRegulars(selectedStates, selectedTypes).then((response) => {
        globalStore.regularsCopy.set(response.data);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedStates, selectedTypes]);

  function clearAllFilters() {
    document
      .getElementsByName("state-check")
      .forEach((el) => (el.checked = false));
    document
      .getElementsByName("type-check")
      .forEach((el) => (el.checked = false));

    setSelectedStates([]);
    setSelectedTypes([]);
  }

  return (
    <Modal
      show={props.showRegularModal}
      onHide={() => props.setShowRegularModal(false)}
      fullscreen={true}>
      <Modal.Header closeButton>
        <Modal.Title>
          Filters{" "}
          <span
            onClick={() => clearAllFilters()}
            className="h6 text-primary text-decoration-underline ms-2">
            Clear All
          </span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="p-0">
        <Row className="m-0">
          <Col xs={3} className="p-0">
            <FilterNav
              logo={<FcGraduationCap />}
              title={"States"}
              active={active === "states"}
              onClick={() => setActive("states")}
            />
            <FilterNav
              logo={<FcLandscape />}
              title={"Types"}
              active={active === "types"}
              onClick={() => setActive("types")}
            />
          </Col>
          <Col xs={9}>
            {active === "states" ? (
              <StateFilter
                states={states}
                selectedStates={selectedStates}
                setSelectedStates={setSelectedStates}
              />
            ) : (
              <TypeFilter
                types={types}
                selectedTypes={selectedTypes}
                setSelectedTypes={setSelectedTypes}
              />
            )}
          </Col>
        </Row>
        {/* <div
          onClick={() => clearAllFilters()}
          className="w-100 position-absolute d-flex align-items-center justify-content-center"
          style={{
            bottom: "0px",
            padding: "16px",
            color: "#0074d7",
            borderTop: "3px solid #f0f0f5",
            cursor: "pointer",
          }}>
          <b>CLEAR ALL</b>
        </div> */}
      </Modal.Body>
    </Modal>
  );
};

export default MobileFilterRegular;
