import React, { useEffect } from "react";
import { useState } from "@hookstate/core";
import { Col, Modal, Row } from "react-bootstrap";

import StateFilter from "../FilterComponents/StateFilter";
import FilterNav from "../FilterNav/FilterNav";
import store from "../../../utils/store";
import {
  getAllVerifiedBoards,
  getFilteredBoards,
  getStatesBoard,
} from "../../../pages/services/Misc";
import { FcGraduationCap } from "react-icons/fc";

const MobileFilterBoard = (props) => {
  const [active, setActive] = React.useState("states");

  const [states, setStates] = React.useState([]);
  const [selectedStates, setSelectedStates] = React.useState([]);

  const globalStore = useState(store);

  function getAllBoards() {
    getAllVerifiedBoards().then((response) => {
      globalStore.boards.set(response.data);
      globalStore.boardsCopy.set(response.data);
    });
  }

  useEffect(() => {
    getStatesBoard().then((response) => setStates(response.data));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (selectedStates.length === 0) {
      getAllBoards();
    } else {
      getFilteredBoards(selectedStates).then((response) => {
        globalStore.boardsCopy.set(response.data);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedStates]);

  function clearAllFilters() {
    document
      .getElementsByName("state-check")
      .forEach((el) => (el.checked = false));

    setSelectedStates([]);
  }

  return (
    <Modal
      show={props.showBoardModal}
      onHide={() => props.setShowBoardModal(false)}
      fullscreen={true}>
      <Modal.Header closeButton>
        <Modal.Title>
          Filters{" "}
          <span
            onClick={() => clearAllFilters()}
            className="h6 text-primary text-decoration-underline ms-2">
            Clear All
          </span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="p-0">
        <Row className="m-0">
          <Col xs={3} className="p-0">
            <FilterNav
              logo={<FcGraduationCap />}
              title={"States"}
              active={active === "states"}
              onClick={() => setActive("states")}
            />
          </Col>
          <Col xs={9}>
            {active === "states" ? (
              <StateFilter
                states={states}
                selectedStates={selectedStates}
                setSelectedStates={setSelectedStates}
              />
            ) : null}
          </Col>
        </Row>
        {/* <div
          onClick={() => clearAllFilters()}
          className="w-100 position-absolute d-flex align-items-center justify-content-center"
          style={{
            bottom: "0px",
            padding: "16px",
            color: "#0074d7",
            borderTop: "3px solid #f0f0f5",
            cursor: "pointer",
          }}>
          <b>CLEAR ALL</b>
        </div> */}
      </Modal.Body>
    </Modal>
  );
};

export default MobileFilterBoard;
