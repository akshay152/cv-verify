import React, { useEffect } from "react";
import { useState } from "@hookstate/core";
import { Col, Modal, Row } from "react-bootstrap";

import ModeFilter from "../FilterComponents/ModeFilter";
import StateFilter from "../FilterComponents/StateFilter";
import TypeFilter from "../FilterComponents/TypeFilter";
import ZoneFilter from "../FilterComponents/ZoneFilter";
import FilterNav from "../FilterNav/FilterNav";
import store from "../../../utils/store";
import {
  getAllUniversities,
  getAllUniversitiesDistance,
  getFilteredUniversities,
  getFilteredUniversitiesDistance,
  getStates,
  getTypes,
} from "../../../pages/services/Misc";
import {
  FcAlphabeticalSortingZa,
  FcGraduationCap,
  FcLandscape,
  FcLibrary,
} from "react-icons/fc";

const MobileFilterDistance = (props) => {
  const [active, setActive] = React.useState("zones");

  //Filter arrays
  const modes = [
    {
      name: "ONLINE",
      description: "Exams and Mode of education will completely be online.",
    },
    {
      name: "DISTANCE",
      description:
        "Exams would happen in university campus but mode of education would be online.",
    },
  ];
  const [selectedModes, setSelectedModes] = React.useState([]);

  const [states, setStates] = React.useState([]);
  const [selectedStates, setSelectedStates] = React.useState([]);

  const [types, setTypes] = React.useState([]);
  const [selectedTypes, setSelectedTypes] = React.useState([]);

  const zones = ["EAST", "WEST", "NORTH", "SOUTH"];
  const [selectedZones, setSelectedZones] = React.useState([]);

  const globalStore = useState(store);

  function getAllUnivsDistance() {
    getAllUniversitiesDistance().then((response) => {
      globalStore.universitiesDistance.set(response.data);
      globalStore.universitiesDistanceCopy.set(response.data);
    });
  }

  useEffect(() => {
    getStates().then((response) => setStates(response.data));
    getTypes().then((response) => setTypes(response.data));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (
      selectedModes.length === 0 &&
      selectedStates.length === 0 &&
      selectedTypes.length === 0 &&
      selectedZones.length === 0
    ) {
      getAllUnivsDistance();
    } else {
      var ss = selectedStates;
      var sz = selectedZones;

      if (selectedStates.length > 0) {
        sz = [];
        document
          .getElementsByName("zone-check")
          .forEach((el) => (el.checked = false));
      }

      if (selectedZones.length > 0) {
        ss = [];
        document
          .getElementsByName("state-check")
          .forEach((el) => (el.checked = false));
      }

      getFilteredUniversitiesDistance(
        selectedModes,
        ss,
        selectedTypes,
        sz
      ).then((response) => {
        globalStore.universitiesDistanceCopy.set(response.data);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedModes, selectedStates, selectedTypes, selectedZones]);

  function clearAllFilters() {
    document
      .getElementsByName("mode-check")
      .forEach((el) => (el.checked = false));
    document
      .getElementsByName("state-check")
      .forEach((el) => (el.checked = false));
    document
      .getElementsByName("type-check")
      .forEach((el) => (el.checked = false));
    document
      .getElementsByName("zone-check")
      .forEach((el) => (el.checked = false));

    setSelectedModes([]);
    setSelectedZones([]);
    setSelectedStates([]);
    setSelectedTypes([]);
  }

  return (
    <Modal
      show={props.showDistanceModal}
      onHide={() => props.setShowDistanceModal(false)}
      fullscreen={true}>
      <Modal.Header closeButton>
        <Modal.Title>
          Filters{" "}
          <span
            onClick={() => clearAllFilters()}
            className="h6 text-primary text-decoration-underline ms-2 cursor-pointer">
            Clear All
          </span>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="p-0">
        <Row className="m-0">
          <Col xs={3} className="p-0">
            <FilterNav
              logo={<FcAlphabeticalSortingZa />}
              title={"Zones"}
              active={active === "zones"}
              onClick={() => setActive("zones")}
            />
            <FilterNav
              logo={<FcGraduationCap />}
              title={"States"}
              active={active === "states"}
              onClick={() => setActive("states")}
            />
            <FilterNav
              logo={<FcLandscape />}
              title={"Types"}
              active={active === "types"}
              onClick={() => setActive("types")}
            />
          </Col>
          <Col xs={9}>
            {active === "modes" ? (
              <ModeFilter
                modes={modes}
                selectedModes={selectedModes}
                setSelectedModes={setSelectedModes}
              />
            ) : active === "states" ? (
              <StateFilter
                states={states}
                selectedStates={selectedStates}
                setSelectedStates={setSelectedStates}
              />
            ) : active === "types" ? (
              <TypeFilter
                types={types}
                selectedTypes={selectedTypes}
                setSelectedTypes={setSelectedTypes}
              />
            ) : (
              <ZoneFilter
                zones={zones}
                selectedZones={selectedZones}
                setSelectedZones={setSelectedZones}
              />
            )}
          </Col>
        </Row>
        {/* <div onClick={() => clearAllFilters()} className='w-100 position-absolute d-flex align-items-center justify-content-center' style={{ bottom: "0px", padding: "16px", color: "#0074d7", borderTop: "3px solid #f0f0f5", cursor: "pointer" }}>
                    <b>CLEAR ALL</b>
                </div> */}
      </Modal.Body>
    </Modal>
  );
};

export default MobileFilterDistance;
