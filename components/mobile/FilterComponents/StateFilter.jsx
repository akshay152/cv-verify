import React from "react";
import { Col, Form, Row } from "react-bootstrap";

const StateFilter = (props) => {
  return (
    <Row className="mt-2 pb-5">
      {props.states.map((state) => (
        <Col xs={6} key={state.id} style={{ fontSize: "14px" }}>
          <Form.Check
            name="state-check"
            defaultChecked={props.selectedStates.includes(state.id.toString())}
            label={state.state}
            value={state.id}
            onChange={(e) => {
              if (e.target.checked) {
                props.setSelectedStates((states) => [
                  ...states,
                  e.target.value,
                ]);
              } else {
                props.setSelectedStates(
                  props.selectedStates.filter(
                    (state) => state !== e.target.value
                  )
                );
              }
            }}
          />
        </Col>
      ))}
    </Row>
  );
};

export default StateFilter;
