import React from "react";
import { Col, Form, Row } from "react-bootstrap";

const TypeFilter = (props) => {
  return (
    <Row className="mt-2 pb-5">
      {props.types.map((type) => (
        <Col xs={6} key={type.type} style={{ fontSize: "14px" }}>
          <Form.Check
            name="type-check"
            defaultChecked={props.selectedTypes.includes(type.type)}
            label={type.type.toUpperCase()}
            value={type.type}
            onChange={(e) => {
              if (e.target.checked) {
                props.setSelectedTypes((types) => [...types, e.target.value]);
              } else {
                props.setSelectedTypes(
                  props.selectedTypes.filter((type) => type !== e.target.value)
                );
              }
            }}
          />
        </Col>
      ))}
    </Row>
  );
};

export default TypeFilter;
