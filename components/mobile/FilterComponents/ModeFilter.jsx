import React from "react";
import { Col, Form, OverlayTrigger, Popover, Row } from "react-bootstrap";
import { TiInfoOutline } from "react-icons/ti";

const ModeFilter = (props) => {
  return (
    <Row className="mt-2 pb-5">
      {props.modes.map((mode) => (
        <Col
          xs={6}
          key={mode.name}
          className="d-flex align-items-center"
          style={{ gap: "6px" }}>
          <Form.Check
            name="mode-check"
            defaultChecked={props.selectedModes.includes(mode.name)}
            value={mode.name}
            label={mode.name}
            onChange={(e) => {
              if (e.target.checked) {
                props.setSelectedModes((modes) => [...modes, e.target.value]);
              } else {
                props.setSelectedModes(
                  props.selectedModes.filter((mode) => mode !== e.target.value)
                );
              }
            }}
          />

          <OverlayTrigger
            trigger="click"
            placement="bottom"
            overlay={
              <Popover id="popover-basic">
                <Popover.Body>{mode.description}</Popover.Body>
              </Popover>
            }>
            <span>
              <TiInfoOutline
                color={mode.name === "ONLINE" ? "#ffc107" : "red"}
                style={{ marginTop: "-6px", cursor: "pointer" }}
              />
            </span>
          </OverlayTrigger>
        </Col>
      ))}
    </Row>
  );
};

export default ModeFilter;
