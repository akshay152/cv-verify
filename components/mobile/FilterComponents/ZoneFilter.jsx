import React from "react";
import { Col, Form, Row } from "react-bootstrap";

const ZoneFilter = (props) => {
  return (
    <Row className="mt-2 pb-5">
      {props.zones.map((zone) => (
        <Col xs={6} key={zone}>
          <Form.Check
            name="zone-check"
            defaultChecked={props.selectedZones.includes(zone)}
            label={zone}
            value={zone}
            onChange={(e) => {
              if (e.target.checked) {
                props.setSelectedZones((zones) => [...zones, e.target.value]);
              } else {
                props.setSelectedZones(
                  props.selectedZones.filter(
                    (zones) => zones !== e.target.value
                  )
                );
              }
            }}
          />
        </Col>
      ))}
    </Row>
  );
};

export default ZoneFilter;
