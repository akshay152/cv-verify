import Link from "next/link";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import expert1 from "/public/images/experts/expert-team1.png";
import expert2 from "/public/images/experts/expert-team2.jpeg";
import expert3 from "/public/images/experts/expert-team3.jpeg";
import expert4 from "/public/images/experts/expert-team4.jpeg";
import expert5 from "/public/images/experts/expert-team5.jpeg";
import expert6 from "/public/images/experts/expert-team6.jpeg";
import {
  BsFillEnvelopeFill,
  BsFillGeoFill,
  BsFillTelephonePlusFill,
  BsGear,
  BsHouseDoor,
} from "react-icons/bs";
import { generate } from "shortid";

const expertlist = [
  {
    image: expert1,
    alt: "manish",
  },
  {
    image: expert2,
    alt: "sarthak",
  },
  {
    image: expert3,
    alt: "shubham",
  },
  {
    image: expert4,
    alt: "sankalp",
  },
  {
    image: expert6,
    alt: "pooja",
  },
  {
    image: expert5,
    alt: "preeti",
  },
];

const OurExpert = () => {
  return (
    <>
      <Container>
        <Row className="my-3 my-md-5">
          <Col md={7}>
            <p className="m-0 fw-bold text-dark" style={{ fontSize: "24px" }}>
              Talk to our Experts
            </p>

            <div
              className="mt-4 mb-3 px-3 py-4 rounded d-inline-block position-relative"
              style={{ backgroundColor: "#f5f5f5" }}
            >
              <span className="position-absolute ms-3 bg-dark start-0 top-0 translate-middle-y fs-12 rounded px-3 py-1 text-white shadow-1">
                {" "}
                Support
              </span>
              <p className="mb-3">
                <span>
                  <BsFillTelephonePlusFill /> New Student :
                </span>{" "}
                <Link href="tel:18004205757">
                  <a className="text-dark fw-bold d-block d-sm-inline">
                    1800-420-5757
                  </a>
                </Link>
              </p>
              <p className="mb-3">
                <span>
                  <BsGear /> Existing Student :
                </span>{" "}
                <Link href="tel:18003097947">
                  <a className="text-dark fw-bold d-block d-sm-inline">
                    1800-309-7947
                  </a>
                </Link>
              </p>
              <p>
                <span>
                  <BsFillEnvelopeFill /> Email :{" "}
                </span>
                <Link href="mailTo:cvcare@collegevidya.com">
                  <a className="text-dark  d-block d-sm-inline fw-bold">
                    {" "}
                    cvcare@collegevidya.com
                  </a>
                </Link>
              </p>
              <p>
                <Link href="/counseling-center/">
                  <a className="text-dark">
                    <BsFillGeoFill /> Visit Us :{" "}
                  </a>
                </Link>
                <Link href="/counseling-center/">
                  <a className="text-dark  d-block d-sm-inline fw-bold">
                    {" "}
                    (10 AM to 7 PM)
                  </a>
                </Link>
              </p>
              <p className="mb-0">
                <Link href="/home-visit/">
                  <a className="text-dark">
                    <BsHouseDoor /> Book :{" "}
                  </a>
                </Link>
                <Link href="/home-visit/">
                  <a className="text-dark  d-block d-sm-inline fw-bold">
                    {" "}
                    Home Visit
                  </a>
                </Link>
              </p>
            </div>
          </Col>
          <Col md={5}>
            <Row>
              {expertlist.map((list) => (
                <Col key={generate()} md={4} xs={4}>
                  <Image
                    src={list.image}
                    width={170}
                    height={170}
                    alt={list.alt}
                  />
                </Col>
              ))}
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default OurExpert;
