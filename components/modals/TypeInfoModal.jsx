import React from "react";
import { FaTimes } from "react-icons/fa";
import { Modal } from "react-bootstrap";

const TypeInfoModal = (props) => {
  return (
    <Modal.Body className="px-4 pb-5 bg-light">
      <div
        style={{
          overflow: "hidden",
          borderRadius: "8px",
        }}
      >
        <div className="d-flex align-items-center justify-content-end">
          <div
            className="d-flex align-items-center justify-content-center"
            style={{
              height: "30px",
              width: "30px",
              borderRadius: "50%",

              cursor: "pointer",
            }}
            onClick={() => props.closeModal()}
          >
            <FaTimes />
          </div>
        </div>
        <h3 className="text-center">Types of Universities</h3>
        <div className="mt-3">
          <div>
            <p
              className="bg-white p-4 border"
              style={{ fontSize: "14px", borderRadius: "12px" }}
            >
              {" "}
              <span className="fw-bold">1. Central Universities</span> are
              basically union universities established under the act of
              Parliament and MoE. There are 40 universities of them that are
              funded by UGC and MoE whereas 9 are autonomous that are directly
              funded by the Government of India (GoI). We have around 54 Central
              universities in India as of now. Central University follows the
              rules &amp; regulations of MHRD.
            </p>
            <p
              className="bg-white p-4 border"
              style={{ fontSize: "14px", borderRadius: "12px" }}
            >
              {" "}
              <span className="fw-bold">2. “Deemed”</span> is a status of
              autonomy granted to a university by DHE under MHRD, on the advice
              of UGC. Deemed to be Universities enjoy full autonomy or
              independence in deciding courses, syllabus, admissions and fees.
              Some Deemed to be Universities can also grant degrees on their
              own. Deemed-to-be Universities, which continue to perform well,
              can get the status of a full-fledged University. According to a
              UGC list, there are 123 Deemed-to-be universities in the country.
            </p>
            <p
              className="bg-white p-4 border"
              style={{ fontSize: "14px", borderRadius: "12px" }}
            >
              {" "}
              <span className="fw-bold">3. Private University</span> are the
              universities that are not operated by the government and are
              managed by some private entity. They are highly dependent on
              student tuition fees, funds, alumni donations, or some private
              fund donors.
            </p>
            <p
              className="bg-white p-4 border"
              style={{ fontSize: "14px", borderRadius: "12px" }}
            >
              {" "}
              <span className="fw-bold">4. State Open Universities</span> set up
              by the respective state grants (as per details below), are single
              mode institutions, which means they provide education only in the
              distance mode. These universities cater to people who are unable
              to pursue regular courses due to various reasons. But however, not
              all open universities focus on distance education These
              Universities are also instrumental in shaping the career growth of
              learners who are already employed.
            </p>
            <p
              className="bg-white p-4 border"
              style={{ fontSize: "14px", borderRadius: "12px" }}
            >
              {" "}
              <span className="fw-bold">5. State University</span> are run by
              State Act or Provincial Act are called State universities. It
              follows the State University Guidelines such as the admission
              criteria, examinations, fees, appointments, etc.
            </p>
          </div>
        </div>
      </div>
    </Modal.Body>
  );
};

export default TypeInfoModal;
