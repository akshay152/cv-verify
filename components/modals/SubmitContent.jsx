import React from "react";
import { Modal } from "react-bootstrap";
import { FaTimes } from "react-icons/fa";
import Image from "next/image";
import anim_image from "../../public/images/report.png";

const SubmitContent = (props) => {
  return (
    <Modal.Body className="shadow-sm border-0" style={{ padding: "0" }}>
      <div className="d-flex align-items-center justify-content-end">
        <div
          className="d-flex align-items-center justify-content-center mt-2 me-2"
          style={{
            height: "30px",
            width: "30px",
            borderRadius: "50%",
            backgroundColor: "#e6e6e6",
            cursor: "pointer",
            color: "#000",
          }}
          onClick={() => props.closeModal()}>
          <FaTimes />
        </div>
      </div>

      <div className="pt-2 pb-5 px-3 text-center">
        <Image src={anim_image} width={50} height={50} alt="" />
        <h3 className="mt-2" style={{ letterSpacing: "1px" }}>
          Be in touch
        </h3>
        <p className="pt-2" style={{ letterSpacing: "1px" }}>
          Thank you for reporting an issue to us. We will look into the same and
          will resolve the issue With in{" "}
          <span className="fw-bold d-block">24 Hours</span>
        </p>
      </div>
      {/* <div className="bg-white rounded-bottom text-dark text-center py-3 shadow-sm" style={{letterSpacing:"1px"}}>With in <span className="fw-bold">24 Hours</span></div> */}
    </Modal.Body>
  );
};

export default SubmitContent;
