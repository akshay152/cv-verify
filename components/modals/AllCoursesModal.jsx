import Image from "next/image";
import Link from "next/link";
import {
  Badge,
  Col,
  Modal,
  OverlayTrigger,
  Popover,
  Row,
} from "react-bootstrap";
import { TiInfoOutline } from "react-icons/ti";
import { generate } from "shortid";
import { FaTimes } from "react-icons/fa";

import AnimatedBird from "../global/AnimatedBird/AnimatedBird";
import store from "../../utils/store";
import { useState } from "@hookstate/core";
import { FcAssistant, FcReading } from "react-icons/fc";

const AllCoursesModal = (props) => {
  const { universalLogo, tagLine } = useState(store);
  const modes = [
    {
      name: "ONLINE",
      description: "Exams and Mode of education will completely be online.",
    },
    {
      name: "DISTANCE",
      description:
        "Exams would happen in university campus but mode of education would be online.",
    },
  ];

  const badgeColor = [
    {
      tag: "CENTRAL UNIVERSITY",
      variant: "success",
    },
    {
      tag: "DEEMED TO BE UNIVERSITY",
      variant: "warning",
    },
    {
      tag: "PRIVATE UNIVERSITY",
      variant: "info",
    },
    {
      tag: "STATE OPEN UNIVERSITY",
      variant: "dark",
    },
    {
      tag: "STATE UNIVERSITY",
      variant: "secondary",
    },
  ];

  return (
    <>
      <Modal.Body className="pb-5" style={{ padding: "16px" }}>
        <Row>
          <Col className="d-flex justify-content-between">
            <div className="d-flex align-items-center" style={{ gap: "6px" }}>
              <Image
                src={universalLogo.get()}
                width={110}
                height={50}
                quality={100}
                objectFit="contain"
                alt="Logo"
              />
              <span style={{ fontSize: "12px" }}>{tagLine.get()}</span>
            </div>
            <div
              onClick={() => props.closeModal()}
              className="d-flex align-items-center justify-content-center"
              style={{
                cursor: "pointer",
                width: "30px",
                height: "30px",
                borderRadius: "50%",
                background: "#e6e6e6",
              }}
            >
              <FaTimes color="gray" />
            </div>
          </Col>
        </Row>
        <div
          className="d-flex align-items-center bg-light px-2 py-3 shadow-sm mb-3 flex-wrap"
          style={{ gap: "12px", borderRadius: "12px" }}
        >
          {props.data.image && props.data.image !== "" ? (
            <div>
              <Image
                className="rounded"
                src={props.data.image}
                height={80}
                width={120}
                alt="University Image"
              />
            </div>
          ) : (
            <Image
              className="rounded"
              src="/images/university_placeholder.png"
              height={80}
              width={120}
              alt="University Image"
            />
          )}
          <div>
            <Row>
              <Col>
                <div style={{ fontSize: "16px" }}>
                  <b>{props.data.name}</b>
                </div>
              </Col>
            </Row>

            <span
              role="button"
              class="mt-2 rounded-pill bg-white text-dark fw-normal badge bg-primary"
              style={{ fontSize: "11px" }}
            >
              {props.data.type}
            </span>

            <div
              className="mt-0"
              style={{ width: "fit-content", cursor: "pointer" }}
            >
              <OverlayTrigger
                trigger="click"
                placement="top"
                overlay={
                  <Popover id="popover-basic">
                    <Popover.Body>
                      {
                        modes.filter((mode) => mode.name === props.data.mode)[0]
                          .description
                      }
                    </Popover.Body>
                  </Popover>
                }
              >
                <div
                  className="d-flex align-items-center py-2"
                  style={{
                    color: "#535766",
                    gap: "6px",
                    fontSize: "12px",
                  }}
                >
                  <FcReading fontSize={18} />
                  <div>
                    Mode of education/exam :{" "}
                    <span className="fw-bold text-success">
                      {" "}
                      {props.data.mode}
                    </span>
                  </div>
                </div>
              </OverlayTrigger>
            </div>
            <div
              className="mt-0"
              style={{ width: "fit-content", cursor: "pointer" }}
            >
              <div
                className="d-flex align-items-center py-2 flex-wrap gap-2"
                style={{
                  color: "#535766",
                  gap: "6px",
                  fontSize: "12px",
                }}
              >
                <FcAssistant fontSize={18} />
                Free Counselling :{" "}
                <Link
                  href={
                    "https://chats.collegevidya.com/counsellors?course=1&page=1&limit=10"
                  }
                >
                  <a target="_blank">
                    <span className="fw-normal bgprimary text-white rounded-pill position-relative connect_now_modal_btn">
                      <span
                        className="fw-bold shadow position-absolute top-100 start-50 translate-middle px-2 text-center rounded-pill"
                        style={{
                          fontSize: "10px",
                          width: "max-content",
                          backgroundColor: "#1abc9c",
                        }}
                      >
                        500+ Expert
                      </span>
                      Connect Now{" "}
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="16"
                        height="16"
                        fill="currentColor"
                        className="bi bi-arrow-right"
                        viewBox="0 0 16 16"
                      >
                        <path
                          fillRule="evenodd"
                          d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"
                        />
                      </svg>
                    </span>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <Row className="mt-1 row-cols-1 row-cols-sm-2 row-cols-md-3">
          {props.data.programmes.split("\n").map((course) => (
            <Col className="mt-2 mb-3" key={generate()}>
              {props.coursesLinks.some((cl) => course.includes(cl.name)) ? (
                <div
                  className="border h-100 position-relative overflow-hidden py-3 px-2 text-center"
                  style={{ borderRadius: "12px" }}
                >
                  {" "}
                  <Link
                    href={
                      props.coursesLinks.filter((cl) =>
                        course.includes(cl.name)
                      )[0].link
                    }
                    passHref
                  >
                    <a className="text-decoration-none" target="_blank">
                      <div className="text-center">
                        <Image
                          src="/images/dummy.png"
                          width={20}
                          height={20}
                          alt=""
                        />

                        <div
                          className="fw-bold text-center mt-2 pb-3 text-dark"
                          style={{ fontSize: "10px" }}
                        >
                          {course}
                        </div>
                        {props.coursesLinks.filter((cl) =>
                          course.includes(cl.name)
                        ).length > 0 ? (
                          <p
                            className="bgprimary text-white mt-2 px-2 py-1 rounded-bottom position-absolute start-0 bottom-0 m-0 mt-2 w-100 text-center"
                            style={{ fontSize: "10px" }}
                          >
                            Compare{" "}
                            <span>
                              {props.coursesLinks.filter((cl) =>
                                course.includes(cl.name)
                              ).length > 0
                                ? props.coursesLinks.filter((cl) =>
                                    course.includes(cl.name)
                                  )[0].count
                                : "null"}
                            </span>{" "}
                            Universities
                          </p>
                        ) : null}
                      </div>
                    </a>
                  </Link>
                </div>
              ) : (
                <div
                  className="border h-100 position-relative overflow-hidden py-3 px-2 text-center"
                  style={{ borderRadius: "12px" }}
                >
                  <Image
                    src="/images/dummy.png"
                    width={20}
                    height={20}
                    alt=""
                  />

                  <div
                    className="fw-bold text-center mt-2 text-dark"
                    style={{ fontSize: "10px" }}
                  >
                    {course}
                  </div>
                </div>
              )}
            </Col>
          ))}
        </Row>
      </Modal.Body>
    </>
  );
};

export default AllCoursesModal;
