import React, { useState } from "react";
import { Button, Col, Form, Modal, Row, Spinner } from "react-bootstrap";

import { useForm } from "react-hook-form";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { FaTimes } from "react-icons/fa";
import { submitIssue } from "../../pages/services/Misc";
import toast from "react-hot-toast";
import Logo from '../../components/global/Logo'
import { MdOutlineReportProblem } from "react-icons/md";
import { AiFillLock } from 'react-icons/ai'

const ReportIssue = (props) => {
  const [loading, setLoading] = useState(false);

  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Name is required"),
    email: Yup.string().email("Invalid email").required("Email is required"),
    number: Yup.number()
      .typeError("Invalid number")
      .required("Mobile number is required"),
    message: Yup.string().required("Issue is required"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = (data) => {
    setLoading(true);
    submitIssue(data)
      .then(() => {
        setLoading(false);
        toast.success("Issue submitted");
        props.endModal();
      })
      .catch(() => {
        setLoading(false);
        toast.error("Oops! Something went wrong");
      });
  };

  const report_heading = {
    backgroundColor: "aliceblue",
    marginTop: "10px",
    marginBottom: "25px",
    textAlign: "center",
    padding: "10px",
    boxShadow: "0 1px 3px 0 rgb(0 0 0 / 17%)",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    gap : "6px",
  };

  return (
    <Modal.Body style={{ padding: "16px" }}>
      <div className="d-flex align-items-center justify-content-between">
      <Logo/>
        <div
          className="d-flex align-items-center justify-content-center"
          style={{
            height: "30px",
            width: "30px",
            borderRadius: "50%",
            backgroundColor: "#e6e6e6",
            cursor: "pointer",
          }}
          onClick={() => props.closeModal()}>
          <FaTimes />
        </div>
      </div>
      <h5 style={report_heading}> <MdOutlineReportProblem /> Report an Issue</h5>
     
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Row>
          <Col>
            <Form.Label>
              Full Name<span style={{ color: "red" }}>*</span>
            </Form.Label>
            <Form.Control {...register("name")} />

            <span className="p-error text-xs">{errors.name?.message}</span>
          </Col>
        </Row>

        <Row className="mt-2">
          <Col>
            <Form.Label>
              Mobile Number<span style={{ color: "red" }}>*</span>
            </Form.Label>
            <Form.Control {...register("number")}  />

            <span className="p-error text-xs">{errors.number?.message}</span>
          </Col>
        </Row>

        <Row className="mt-2">
          <Col>
            <Form.Label>
              Email Address<span style={{ color: "red" }}>*</span>
            </Form.Label>
            <Form.Control
              {...register("email")}
              
            />

            <span className="p-error text-xs">{errors.email?.message}</span>
          </Col>
        </Row>

        <Row className="mt-2">
          <Col>
            <Form.Label>
              Issue<span style={{ color: "red" }}>*</span>
            </Form.Label>
            <Form.Control as="textarea" rows={4} {...register("message")} />

            <span className="p-error text-xs">{errors.message?.message}</span>
          </Col>
        </Row>
        <div style={{backgroundColor:"#f5f5dc"}} className="px-3 py-2">
        <small>Note : </small>
        <p className="fw-normal d-flex align-items-center gap-1" style={{fontSize:"12px"}}><AiFillLock/> Your information will be kept confidential</p>
        </div>

        <Row className="mt-3">
          <Col className="text-end">
            {loading ? (
              <Button>
                <Spinner animation="border" size="sm" />
              </Button>
            ) : (
              <Button type="submit">Submit</Button>
            )}
          </Col>
        </Row>
      </Form>
    </Modal.Body>
  );
};

export default ReportIssue;
