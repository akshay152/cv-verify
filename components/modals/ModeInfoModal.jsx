import { Modal } from "react-bootstrap";
import { FaTimes, FaPlay } from "react-icons/fa";
import Link from "next/link";

const ModeInfoModal = (props) => {
  return (
    <Modal.Body className="p-0">
      <div
        style={{
          padding: "16px 16px 60px 16px",
          backgroundColor: "#FBE7D0",
          overflow: "hidden",
          borderRadius: "8px",
        }}>
        <div className="d-flex align-items-center justify-content-end">
          <div
            className="d-flex align-items-center justify-content-center"
            style={{
              height: "30px",
              width: "30px",
              borderRadius: "50%",
              backgroundColor: "#e6e6e6",
              cursor: "pointer",
            }}
            onClick={() => props.closeModal()}>
            <FaTimes />
          </div>
        </div>
        <div style={{ fontSize: "24px", fontWeight: "bold" }}>
          3 Major Differences Between Online Education &amp; Distance Education
        </div>
        <p className="mt-1">
          In the Era of “New Normal”, this mode of education is taking new hype
          So every new way comes up with a lot of confusion and misconceptions.
          Therefore today, we wanted to clear this one doubt about the
          difference between online education &amp; distance education. Here are
          the 3 major differences between online &amp; distance education.
        </p>
        <ol>
          <li>
            <span style={{ textDecoration: "underline", color: "#008080" }}>
              Difference of Approvals:
            </span>
            &nbsp;In online education, the university requires NAAC
            accreditation and NIRF ranking whereas in distance education the
            university requires UGC-DEB approval.
          </li>
          <li>
            <span style={{ textDecoration: "underline", color: "#008080" }}>
              Examination Mode:
            </span>
            &nbsp;In Online Education both studies and examinations held online
            and in distance examinations will be in the university center.
          </li>
          <li>
            <span style={{ textDecoration: "underline", color: "#008080" }}>
              Difference of LMS:
            </span>
            &nbsp;In distance education, most of the university except some top
            universities does not offer live lectures and other facilities but
            online education inculcates all the advantages including live
            lectures &amp; assignments.
          </li>
        </ol>
        <div className="mt-4 d-flex align-items-center" style={{ gap: "6px" }}>
          To know more students can watch{" "}
          <span className="pulse">
            <Link href="https://www.youtube.com/watch?v=wboVla7eJ7A" passHref>
              <a target="_blank" style={{ textDecoration: "none" }}>
                <FaPlay color="white" />
              </a>
            </Link>
          </span>
        </div>
      </div>
    </Modal.Body>
  );
};

export default ModeInfoModal;
