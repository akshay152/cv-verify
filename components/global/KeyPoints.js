import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { BsBank, BsPen, BsSearch, BsShieldCheck } from "react-icons/bs";

const KeyPoints = () => {
  const data = [
    {
      icon: <BsPen />,
      title: "Enter Your University",
      description:
        " Please enter the name of your university in the search box.",
      bgColor: "#EAE6FF",
      iconColor: "#8f81e1",
    },
    {
      icon: <BsSearch />,
      title: "search Your University",
      description:
        " Please enter the name of your university in the search box.",
      bgColor: "#CBF9E1",
      iconColor: "#55ae7f",
    },

    {
      icon: <BsShieldCheck />,
      title: "Check Approvals ",
      description:
        " Please enter the name of your university in the search box.",
      bgColor: "#feebe7",
      iconColor: "#ae7264",
    },
    {
      icon: <BsBank />,
      title: "Get Your Result",
      description:
        " Please enter the name of your university in the search box.",
      bgColor: "#FFF2BC",
      iconColor: "#bdaa5b",
    },
  ];

  return (
    <>
      <Row className="mt-2 mt-sm-4 row-cols-1 row-cols-sm-2 row-cols-lg-4">
        {data.map((list, index) => (
          <Col key={index} className="mb-4">
            <div
              className="p-4 p-xl-5 h-100"
              style={{ backgroundColor: list.bgColor, borderRadius: "12px" }}
            >
              <p
                style={{
                  fontSize: "24px",
                  color: list.iconColor,
                }}
              >
                {list.icon}
              </p>
              <p className="fw-bold mb-2" style={{ fontSize: "16px" }}>
                {list.title}
              </p>
              <p style={{ fontSize: "14px" }}>{list.description}</p>
            </div>
                </Col>
                
        ))}
      </Row>
   </>
   
  );
};

export default KeyPoints;
