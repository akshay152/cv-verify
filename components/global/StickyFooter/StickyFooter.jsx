import React, { useEffect } from "react";
import Link from "next/link";
import { Col, Row } from "react-bootstrap";
import { AiOutlineMenu } from "react-icons/ai";
import { FaUniversity } from "react-icons/fa";

import styles from "./StickyFooter.module.css";
import { useState } from "@hookstate/core";
import store from "../../../utils/store";

const StickyFooter = (props) => {
  const globalStore = useState(store);

  return (
    <div className={`d-block d-lg-none ${styles.main_div}`}>
      <Row>
        {globalStore.activeIndex.get() == 0 && (
          <Col
            onClick={() => props.setShowModal(true)}
            xs={6}
            className={`text-center  pe-0 pe-1`}>
            <div className={`${styles.button}`} style={{ fontSize: "12px" }}>
              <AiOutlineMenu color="white" />
              Filters
            </div>
          </Col>
        )}

        {globalStore.activeIndex.get() == 1 && (
          <Col
            onClick={() => props.setShowDistanceModal(true)}
            xs={6}
            className={`text-center  pe-0 pe-1`}>
            <div className={`${styles.button}`} style={{ fontSize: "12px" }}>
              <AiOutlineMenu color="white" />
              Filters
            </div>
          </Col>
        )}

        {globalStore.activeIndex.get() == 3 && (
          <Col
            onClick={() => props.setShowBoardModal(true)}
            xs={6}
            className={`text-center  pe-0 pe-1`}>
            <div className={`${styles.button}`} style={{ fontSize: "12px" }}>
              <AiOutlineMenu color="white" />
              Filters
            </div>
          </Col>
        )}

        {globalStore.activeIndex.get() == 4 && (
          <Col
            onClick={() => props.setShowRegularModal(true)}
            xs={6}
            className={`text-center  pe-0 pe-1`}>
            <div className={`${styles.button}`} style={{ fontSize: "12px" }}>
              <AiOutlineMenu color="white" />
              Filters
            </div>
          </Col>
        )}

        <Col
          xs={
            globalStore.activeIndex.get() === 2 ||
            globalStore.activeIndex.get() === 5
              ? 12
              : 6
          }
          className="text-center ps-0 ps-1">
          <Link
            href="https://collegevidya.com/suggest-me-an-university"
            passHref>
            <a className={styles.link_a} target="_blank">
              <div className={`${styles.button}`} style={{ fontSize: "12px" }}>
                <FaUniversity color="white" />
                {globalStore.activeIndex.get() === 2 ||
                globalStore.activeIndex.get() === 5
                  ? "Suggest University in 2 minutes"
                  : "Suggest University"}
              </div>
            </a>
          </Link>
        </Col>
      </Row>
    </div>
  );
};

export default StickyFooter;
