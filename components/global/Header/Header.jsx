import Image from "next/image";
import Link from "next/link";
import React from "react";
import {
  Container,
  Modal,
  Nav,
  Navbar,
  NavDropdown,
  Offcanvas,
} from "react-bootstrap";
import { HiDownload } from "react-icons/hi";
import {
  FcMenu,
  FcAdvance,
  FcInternal,
  FcCallback,
  FcAssistant,
  FcApprove,
} from "react-icons/fc";
import { BsChevronDown } from "react-icons/bs";
import AnimatedBird from "../AnimatedBird/AnimatedBird";

import styles from "./Header.module.css";
import { BsFileEarmarkPdf } from "react-icons/bs";
import ReportIssue from "../../modals/ReportIssue";
import SubmitContent from "../../modals/SubmitContent";
import { useState } from "@hookstate/core";
import store from "../../../utils/store";

const Header = () => {
  const [showMobMenu, setShowMobMenu] = React.useState(false);

  const [showModal, setShowModal] = React.useState(false);
  const [modalContent, setShowModalContent] = React.useState(null);

  const [showSubmitModal, setShowSubmitModal] = React.useState(false);
  const [submitModalContent, setSubmitModalContent] = React.useState(null);

  const globalStore = useState(store);
  const { universalLogo, tagLine } = useState(store);

  function HeaderModal() {
    return (
      <Modal show={showModal} onHide={() => setShowModal(false)} centered>
        {modalContent}
      </Modal>
    );
  }

  function SubmitModal() {
    return (
      <Modal
        show={showSubmitModal}
        onHide={() => setShowSubmitModal(false)}
        size="sm"
        centered
      >
        {submitModalContent}
      </Modal>
    );
  }

  function setupModal(key) {
    if (key === "report-issue") {
      setShowModalContent(
        <ReportIssue
          closeModal={() => {
            setShowModal(false);
          }}
          endModal={() => {
            setShowModal(false);
            setupModal("submit-issue");
          }}
        />
      );
      setShowModal(true);
    }
    if (key === "submit-issue") {
      setSubmitModalContent(
        <SubmitContent closeModal={() => setShowSubmitModal(false)} />
      );
      setShowSubmitModal(true);
    }
  }

  return (
    <Navbar className={styles.main_wrapper} variant="light" expand="lg">
      <Container>
        <Navbar.Brand className="m-0">
          <div className="d-flex align-items-center" style={{ gap: "6px" }}>
            <div
              onClick={() => setShowMobMenu(true)}
              className={`${styles.mob_menu} d-block d-lg-none`}
              style={{ boxShadow: "6px solid black" }}
            >
              <FcMenu fontSize={20} />
            </div>
            <Link href="https://collegevidya.com">
              <a>
                <div
                  className="d-flex align-items-center justify-content-center"
                  style={{ gap: "6px" }}
                >
                  <img
                    className="w-100 header_logo"
                    src={`/_next/image/?url=${encodeURIComponent(
                      universalLogo.get()
                    )}&w=384&q=75`}
                    objectFit="contain"
                    alt="logo"
                  />
                </div>
              </a>
            </Link>
          </div>
        </Navbar.Brand>
        <Nav
          className="me-auto d-none d-sm-block"
          style={{ paddingLeft: "6px" }}
        >
          <Nav.Item style={{ fontSize: "14px" }}>
            <span>{tagLine.get()}</span>
          </Nav.Item>
        </Nav>
        <Nav className="d-block d-lg-none mob-menu">
          <NavDropdown
            title={
              <div
                className="d-inline-block rounded py-2 px-3 text-white border-secondary bgprimary"
                target="_blank"
                style={{
                  textDecoration: "none",

                  fontSize: "12px",
                }}
              >
                <div
                  className="d-flex align-items-center"
                  style={{ gap: "6px" }}
                >
                  <HiDownload /> UGC PDF
                </div>
              </div>
            }
          >
            <div className="menu-drop">
              <Link href="/documents/ODL_List_2023_24.pdf" passHref>
                <a
                  target="blank"
                  style={{ textDecoration: "none", color: "black" }}
                >
                  Online
                </a>
              </Link>
            </div>
            <div className="menu-drop">
              <Link href="/documents/ODL_List_2023_24.pdf" passHref>
                <a
                  className="w-100"
                  target="blank"
                  style={{ textDecoration: "none", color: "black" }}
                >
                  Distance
                </a>
              </Link>
            </div>
            <div className="menu-drop">
              <Link href="/" passHref>
                <a
                  className="w-100"
                  target="blank"
                  style={{ textDecoration: "none", color: "black" }}
                >
                  Fake Universities
                </a>
              </Link>
            </div>
            <div className="menu-drop">
              <div
                className="w-100"
                target="blank"
                onClick={() => globalStore.activeIndex.set(3)}
                style={{
                  textDecoration: "none",
                  color: "black",
                  cursor: "pointer",
                }}
              >
                Verify Your Past Record
              </div>
            </div>
            <div className="menu-drop">
              <div
                className="w-100"
                target="blank"
                onClick={() => globalStore.activeIndex.set(0)}
                style={{
                  textDecoration: "none",
                  color: "black",
                  cursor: "pointer",
                }}
              >
                Verify Your Future
              </div>
            </div>
          </NavDropdown>
        </Nav>
        <Nav className="align-items-center d-none d-lg-flex">
          <NavDropdown
            title={
              <div
                className="d-inline-block rounded py-2 px-2 text-white border-secondary bgprimary"
                target="_blank"
                style={{
                  textDecoration: "none",
                  color: "#0074d7",
                  fontSize: "12px",
                }}
              >
                <div
                  className="d-flex align-items-center"
                  style={{ gap: "6px" }}
                >
                  Check yourself! (UGC PDF)
                  <BsChevronDown fontSize={14} className="fw-bold" />
                </div>
              </div>
            }
          >
            <div className="menu-drop">
              <Link href="/documents/ODL_List_2023_24.pdf" passHref>
                <a
                  target="blank"
                  style={{
                    textDecoration: "none",
                    color: "black",
                    fontSize: "14px",
                  }}
                >
                  Online
                </a>
              </Link>
            </div>
            <div className="menu-drop">
              <Link href="/documents/ODL_List_2023_24.pdf" passHref>
                <a
                  className="w-100"
                  target="blank"
                  style={{
                    textDecoration: "none",
                    color: "black",
                    fontSize: "14px",
                  }}
                >
                  Distance
                </a>
              </Link>
            </div>
            <div className="menu-drop">
              <Link href="/documents/Fake_Univs.pdf" passHref>
                <a
                  className="w-100"
                  target="blank"
                  style={{
                    textDecoration: "none",
                    color: "black",
                    fontSize: "14px",
                  }}
                >
                  Fake Universities
                </a>
              </Link>
            </div>
            <div className="menu-drop">
              <div
                className="w-100"
                target="blank"
                onClick={() => globalStore.activeIndex.set(3)}
                style={{
                  textDecoration: "none",
                  color: "black",
                  cursor: "pointer",
                  fontSize: "14px",
                }}
              >
                Verify Your Past Record
              </div>
            </div>
            <div className="menu-drop">
              <div
                className="w-100"
                target="blank"
                onClick={() => globalStore.activeIndex.set(0)}
                style={{
                  textDecoration: "none",
                  color: "black",
                  cursor: "pointer",
                  fontSize: "14px",
                }}
              >
                Verify Your Future
              </div>
            </div>
          </NavDropdown>

          <Nav.Item>
            <Link href="https://collegevidya.com/suggest-me-an-university">
              <a
                target="_blank"
                className="suggest-me-btn"
                style={{ fontSize: "12px" }}
              >
                <Image
                  src="/images/badge.png"
                  width={20}
                  height={20}
                  alt="badge"
                />
                Suggest University in 2 mins
                <span className="nav_new text-white">New</span>
              </a>
            </Link>
          </Nav.Item>
        </Nav>
      </Container>
      <Offcanvas show={showMobMenu} onHide={() => setShowMobMenu(false)}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>
            <div>
              <span style={{ marginLeft: "6px", fontSize: "14px" }}>
                <span>#ChunoWahiJoHaiVerified</span>
              </span>
            </div>
          </Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <div className={styles.menu_tag}>Verify Yourself</div>
          <div className="mb-3 mt-3">
            <div
              target="_blank"
              className={styles.menu_a_tag}
              style={{ cursor: "pointer" }}
              onClick={() => {
                globalStore.activeIndex.set(3);
                setShowMobMenu(false);
              }}
            >
              <div
                className="d-flex align-items-center fw-bold"
                style={{ gap: "6px" }}
              >
                <FcAdvance /> Verify Your Past Record
              </div>
            </div>
          </div>
          <div className="mb-3 mt-3">
            <div className="mb-3 mt-3">
              <div
                target="_blank"
                className={styles.menu_a_tag}
                style={{ cursor: "pointer" }}
                onClick={() => {
                  globalStore.activeIndex.set(0);
                  setShowMobMenu(false);
                }}
              >
                <div
                  className="d-flex align-items-center fw-bold"
                  style={{ gap: "6px" }}
                >
                  <FcAdvance /> Verify Your Future
                </div>
              </div>
            </div>
          </div>
          <Link href="/documents/Fake_Univs.pdf" passHref>
            <a target="_blank" className={`${styles.menu_a_tag}`}>
              <div className="d-flex align-items-center" style={{ gap: "6px" }}>
                <FcInternal /> Fake Universities{" "}
                <BsFileEarmarkPdf color="red" />
              </div>
            </a>
          </Link>
          <div className="mb-3 mt-3">
            <Link href="/documents/ODL.pdf" passHref>
              <a target="_blank" className={styles.menu_a_tag}>
                <div
                  className="d-flex align-items-center"
                  style={{ gap: "6px" }}
                >
                  <FcInternal /> Check yourself! (UGC PDF){" "}
                  <BsFileEarmarkPdf color="red" />
                </div>
              </a>
            </Link>
          </div>

          <div className="mb-3">
            <Link
              href="https://collegevidya.com/suggest-me-an-university"
              passHref
            >
              <a
                target="_blank"
                className={`${styles.menu_a_tag}`}
                style={{ display: "flex", alignItems: "center", gap: "6px" }}
              >
                Suggest University in 2 mins
                <span className={styles.menu_new}>New</span>
              </a>
            </Link>
          </div>
          <div className={`${styles.menu_tag}`}>Why Trust Us</div>
          <Link href="https://collegevidya.com/our-trust/" passHref>
            <a target="_blank" className={styles.menu_a_tag}>
              <div className="d-flex align-items-center" style={{ gap: "6px" }}>
                CollegeVidya Commitment
                <Image
                  src="/images/stamp.png"
                  width={53}
                  height={37}
                  alt="Stamp"
                />
              </div>
            </a>
          </Link>
          <div className="mb-3 mt-2">
            <Link href="https://collegevidya.com/collection/" passHref>
              <a target="_blank" className={`${styles.menu_a_tag}`}>
                <div
                  className="d-flex align-items-center"
                  style={{ gap: "6px" }}
                >
                  Important Videos (CV TV)
                </div>
              </a>
            </Link>
          </div>
          <div className={`${styles.menu_tag}`}>Contact Us</div>
          <div className="mb-3">
            <Link href="tel:18004205757">
              <a className={`${styles.menu_a_tag}`}>
                <div
                  className="d-flex align-items-center"
                  style={{ gap: "6px" }}
                >
                  {/* <Image
                    src="/images/telephone.png"
                    width={15}
                    height={15}
                    alt=""
                  /> */}
                  <FcAssistant fontSize={18} />
                  <span>New User: 18004205757</span>
                </div>
              </a>
            </Link>
          </div>
          <div className="mb-3">
            <Link href="tel:18003097947">
              <a className={`${styles.menu_a_tag}`}>
                <div
                  className="d-flex align-items-center"
                  style={{ gap: "6px" }}
                >
                  {/* <Image
                    src="/images/settings.png"
                    width={18}
                    height={18}
                    alt=""
                  /> */}
                  <FcApprove fontSize={18} />
                  <span>Existing User: 18003097947</span>
                </div>
              </a>
            </Link>
          </div>
          <div className={`${styles.menu_tag}`}>Noticed some issues?</div>
          <div
            className="mb-3 d-flex align-items-center"
            style={{ cursor: "pointer" }}
            onClick={() => setupModal("report-issue")}
          >
            <FcCallback fontSize={18} />
            <span className="ms-1">Let us know</span>
          </div>
          <div className={`${styles.menu_tag}`}>Download Mobile App</div>
          <div className="mb-5" style={{ background: "#eee", padding: "8px" }}>
            <Link
              href="https://play.google.com/store/apps/details?id=com.app.collegevidya"
              passHref
            >
              <a target="_blank" className={`${styles.menu_a_tag}`}>
                <div
                  className="d-flex align-items-center"
                  style={{ gap: "6px" }}
                >
                  <Image
                    src="/images/google_play.png"
                    width={100}
                    height={30}
                    alt=""
                  />
                </div>
              </a>
            </Link>
          </div>
        </Offcanvas.Body>
      </Offcanvas>
      <HeaderModal />
      <SubmitModal />
    </Navbar>
  );
};

export default Header;
