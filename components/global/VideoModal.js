import React, { useRef, useState } from "react";
import Modal from "react-bootstrap/Modal";
import AnimatedYoutube from "./AnimatedYoutube";

const VideoModal = ({ title, videoID, banner_video }) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => {
    setShow(true);
    videoClickRef.current.click();
  };

  const videoClickRef = useRef();

  return (
    <>
      {banner_video ? (
        <>
          <p
            onClick={handleShow}
            role="button"
            className="bgprimary m-0 text-white px-4 py-3 rounded d-inline-flex align-items-center gap-2"
          >
            Watch Video <AnimatedYoutube />
          </p>
          <input id="VIDEO-CLICK" type={"hidden"} ref={videoClickRef} />
        </>
      ) : (
        <span onClick={handleShow} className="cursor-pointer">
          <AnimatedYoutube banner_video={true} />
          <input id="VIDEO-CLICK" type={"hidden"} ref={videoClickRef} />
        </span>
      )}

      <Modal
        show={show}
        size={"lg"}
        onHide={handleClose}
        centered
        animation
        style={{ backgroundColor: "rgba(0,0,0,0.9)", zIndex: "1111" }}
      >
        <Modal.Header closeButton className="border-0">
          <Modal.Title className="text-truncate" style={{ fontSize: "14px" }}>
            {title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="p-0 d-flex align-items-center">
          <iframe
            className="embed-responsive-item video_modal_iframe"
            width={"100%"}
            height={"450"}
            src={`https://www.youtube.com/embed/${videoID}`}
            title="YouTube video player"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          />
        </Modal.Body>
      </Modal>
    </>
  );
};

export default VideoModal;
