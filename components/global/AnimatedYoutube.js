import React, { useState } from "react";
import styles from "./AnimatedYoutube.module.css";
import { BsPlayFill } from "react-icons/bs";
import { useRef } from "react";

const AnimatedYoutube = ({ banner_video }) => {
  const videoClickRef = useRef();
  return (
    <>
      <span
        onClick={() => videoClickRef.current.click()}
        className={`${styles.pulse} d-flex justify-content-center align-items-center`}
        style={{ width: banner_video ? "50px" : "20px" }}
      >
        <BsPlayFill className="text-white fs-16" />
      </span>
      <input id="VIDEO-CLICK" type={"hidden"} ref={videoClickRef} />
    </>
  );
};

export default AnimatedYoutube;
