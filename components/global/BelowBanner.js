import React from 'react'
import UniversitySlider from './UniversitySlider';

const BelowBanner = ({ UniversityData }) => {
  return (
    <div className="mt-3 text-center">
      {/* <p className="h2 fw-bold">We collaborate with 325+ Top universities </p> */}
      <UniversitySlider UniversityData={UniversityData} />
    </div>
  );
};

export default BelowBanner