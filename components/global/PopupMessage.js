import React from 'react'
import { Button, Popup } from 'semantic-ui-react'

const PopupMessage = (props) => {
  return (
    <>

<Popup position={props.position} style={{maxWidth:"180px"}} className='text-center'
    trigger={<Button className="ms-1 rounded-circle" style={{padding:"1px",fontSize:"7px",backgroundColor:"#fff",border:"1px solid #ccc"}} icon='info' />}
    content={props.title}
    inverted
  />
    </>
  )
}

export default PopupMessage