import Image from 'next/image'
import React from 'react'
import logo from '/public/images/logo.svg'

const Logo = () => {
  return (
    <>
      <Image src={logo} alt="" />
    </>
  )
}

export default Logo