import React from "react";
import { Tooltip } from "primereact/tooltip";
import { BsInfoCircle } from "react-icons/bs";

const ToolTipOver1 = (props) => {
  return (
    <div>
      <Tooltip target=".custom-target-icon" style={{ fontSize: "14px" }} />
      <span
        className="custom-target-icon"
        data-pr-tooltip={props.tooltipText}
        data-pr-position="right">
        <BsInfoCircle className="ms-1" fontSize={13} />
      </span>
    </div>
  );
};

export default ToolTipOver1;
