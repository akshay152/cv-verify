import React from "react";
import ReactTooltip from "react-tooltip";
import { BsInfoCircle } from "react-icons/bs";

const ToolTipOver = (props) => {
  return (
    <div>
      <span
        data-tip={props.toolTipData}
        data-background-color="#000"
        data-text-color="white"
        data-place="top"
        data-effect="solid">
        <BsInfoCircle className="ms-1" fontSize={13} />
      </span>
      <ReactTooltip />
    </div>
  );
};

export default ToolTipOver;
