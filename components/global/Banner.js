import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import VideoModal from "./VideoModal";

const Banner = () => {
  return (
    <div>
      <Row>
        <Col className="col-12 col-lg-5 d-flex align-items-center order-2 order-lg-1">
          <div>
            <h2 className="banner_heading">
              Verify University Before Applying
            </h2>
            <p className="text-secondary mb-0 mb-lg-3">
              "Don't make a decision that you might regret! Pick UGC-DEB stamped
              universities only for quality education and recognized degrees."
            </p>
            <div className="d-none d-lg-block">
              {" "}
              <VideoModal
                banner_video={true}
                title={"High Alert: क्या आपकी University approved है?"}
                videoID={"Diao6b6lfpE"}
              />
            </div>
          </div>
        </Col>
        <Col className="col-12 col-lg-7 order-1 order-lg-2 mb-4 mb-lg-0">
          <div className="position-relative">
            <div
              role="button"
              className="position-absolute top-50 start-50 translate-middle"
              style={{ zIndex: "1" }}
            >
              <VideoModal
                banner_video={false}
                title={"High Alert: क्या आपकी University approved है?"}
                videoID={"Diao6b6lfpE"}
              />
            </div>
            <Image
              style={{ borderRadius: "12px" }}
              src="/images/fake_list.webp"
              width={750}
              height={400}
              alt="fake university list"
              layout="responsive"
            />
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Banner;
