import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "swiper/css/autoplay";
import "swiper/css/grid";
import { EffectFade, Navigation, Autoplay, Grid, Pagination } from "swiper";
import Modal from "react-bootstrap/Modal";
import Link from "next/link";
import { BsArrowRight, BsX, BsXCircle } from "react-icons/bs";
import Image from "next/image";
import styles from "./UniversitySlider.module.css";

const UniversitySlider = ({ UniversityData }) => {
  return (
    <>
      <div className="px-2 py-2 mt-4">
        <Swiper
          slidesPerView="auto"
          pagination={false}
          spaceBetween={20}
          speed={5000}
          autoplay={{
            delay: 0,
            disableOnInteraction: false,
          }}
          loop={true}
          navigation={false}
          breakpoints={{
            "@0.00": {
              slidesPerView: 2,
            },
            200: {
              slidesPerView: 2,
            },
            300: {
              slidesPerView: 3,
            },

            500: {
              slidesPerView: 4,
            },
            600: {
              slidesPerView: 5,
            },
            800: {
              slidesPerView: 5,
            },
            1000: {
              slidesPerView: 6,
            }, 1200: {
              slidesPerView: 7,
            },
          }}
          modules={[Grid, Navigation, EffectFade, Autoplay, Pagination]}
        >
          {UniversityData.map((list, index) => (
            <SwiperSlide key={index} className="cursor-pointer py-2">
              <Card className="text-center h-100 px-3 py-2 rounded border-0">
                <Image
                  src={list.logo}
                  width={150}
                  height={50}
                  alt=""
                  objectFit="contain"
                  quality={100}
                  priority
                />
                <p
                  className="m-0 text-truncate mt-2"
                  style={{ fontSize: "10px" }}
                >
                  {" "}
                  {list.name}
                </p>
              </Card>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </>
  );
};

export default UniversitySlider;
