import React from "react";
import { FcGlobe } from "react-icons/fc";
import { BsCursorFill } from "react-icons/bs";
import styles from "../../styles/searchCard.module.css";

const SearchCard = (props) => {
  return (
    <>
      <div
        className={`${styles.searchCard} bg-white mb-3 py-3 border h-100 text-center shadow-sm d-flex flex-column justify-content-between`}
        style={{ padding: "10px 20px", borderRadius: "12px" }}
      >
        <div className="me-2">
          {props.image ? (
            // eslint-disable-next-line @next/next/no-img-element
            <img
              src={`/images/boards/${props.image}`}
              width={80}
              height={70}
              style={{ objectFit: "contain" }}
              className="rounded"
              alt=""
            />
          ) : (
            // eslint-disable-next-line @next/next/no-img-element
            <img
              src={`/images/andhra-pradesh-board-of-intermediate-education.jpg`}
              width={80}
              height={70}
              style={{ objectFit: "contain" }}
              className="rounded"
              alt=""
            />
          )}
          <div className="mt-3" style={{ fontSize: "14px" }}>
            <span className="fw-bold">{props.name}</span>
          </div>
        </div>
        <div className="hello">
          <div className="d-flex justify-content-between mt-3 mb-0 flex-wrap">
            {" "}
            <div
              className="d-flex align-items-center"
              style={{ color: "#535766", gap: "3px", fontSize: "12px" }}
            >
              <FcGlobe fontSize={12} />
              <div>{props.state}</div>
            </div>
            <div
              className="d-flex align-items-center"
              style={{ color: "#535766", gap: "3px", fontSize: "12px" }}
            >
              <BsCursorFill fontSize={12} />
              <div>Code : {props.code}</div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SearchCard;
