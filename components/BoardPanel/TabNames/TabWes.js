import React from "react";
import PopupMessage from "../../global/PopupMessage";

const TabWes = () => {
  return (
    // <div className='d-flex'>WES<ToolTipOver1 tooltipText = "Wes Approved - Degree Internationally Accepted ✅"/></div>

    <div className="d-flex">
      WES{" "}
      <PopupMessage title="WES Approved- Degree Internationally Accepted ✅" />
    </div>
  );
};

export default TabWes;
