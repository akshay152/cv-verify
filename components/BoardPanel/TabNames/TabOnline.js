import React from "react";
import PopupMessage from "../../global/PopupMessage";

const TabOnline = () => {
  return (
    <div className="d-flex">
      Online
      <PopupMessage title="Online Universities in India. Exams and Mode of education will completely be online." />
    </div>
  );
};

export default TabOnline;
