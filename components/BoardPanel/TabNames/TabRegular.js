import React from "react";
import PopupMessage from "../../global/PopupMessage";

const TabRegular = () => {
  return (
    <div className="d-flex">
      Regular <PopupMessage title="Universities Approved for Regular Mode" />    </div>
  );
};

export default TabRegular;
