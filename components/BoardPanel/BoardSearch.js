import React from "react";
import { ReactSearchAutocomplete } from "react-search-autocomplete";
import { Badge } from "react-bootstrap";
import store from "../../utils/store";

const BoardSearch = () => {
  const globalStore = useState(store);

  return (
    <>
      <ReactSearchAutocomplete
        onSearch={handleOnSearch}
        onClear={() => getAllUnivs()}
        placeholder="Is your School Board VALID? Search..."
        onSelect={handleOnSelect}
      />
      <span
        className="ps-3 mt-2 fw-bold mb-3 d-block"
        style={{ fontSize: "12px" }}>
        About {globalStore.boardsCopy.attach(Downgraded).get().length} results (
        {reqTime} seconds)
      </span>
    </>
  );
};

export default BoardSearch;
