import { useEffect, useState } from "react";
import Typist from "react-typist";

const AnimatedText = (props) => {
  const [count, setCount] = useState(1);

  useEffect(() => {
    setCount(1);
  }, [count]);

  return (
    <>
      {count ? (
        <Typist avgTypingDelay={70} onTypingDone={() => setCount(0)}>
          {props.texts.map((text) => (
            <span key={text}>
              <span>{text}</span>
              <Typist.Backspace count={text.length} delay={800} />
            </span>
          ))}
        </Typist>
      ) : (
        ""
      )}
    </>
  );
};

export default AnimatedText;
