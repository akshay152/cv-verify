import React from "react";
import { FcGlobe } from "react-icons/fc";
import { BsCursorFill } from "react-icons/bs";
import { Badge } from "react-bootstrap";
import styles from "../../styles/searchCard.module.css";

const RegularSearchCard = (props) => {
  return (
    <>
      <div
        className={`${styles.searchCard} bg-white mb-3 py-3 border h-100 shadow-sm d-flex flex-column justify-content-between`}
        style={{ padding: "10px 20px", borderRadius: "12px" }}
      >
        <div>
          {props.image ? (
            // eslint-disable-next-line @next/next/no-img-element
            <img
              src={"/images/regulars/" + props.image}
              width={80}
              height={70}
              style={{ objectFit: "contain" }}
              className="rounded"
              alt=""
            />
          ) : (
            // eslint-disable-next-line @next/next/no-img-element
            <img
              src={`/images/university_placeholder.png`}
              width={80}
              height={70}
              style={{ objectFit: "contain" }}
              className="rounded"
              alt=""
            />
          )}
          <div style={{ fontSize: "14px" }} className="fw-bold mt-3">
            <span>{props.name}</span>
          </div>
        </div>
        <div className="hello mt-3">
          <div className="d-flex justify-content-between flex-wrap">
            {" "}
            <div
              className="d-flex align-items-center mb-1"
              style={{ color: "#535766", gap: "3px", fontSize: "12px" }}
            >
              <FcGlobe fontSize={12} />
              <div>{props.state}</div>
            </div>
            <div
              className="d-flex align-items-center mb-1"
              style={{ color: "#535766", gap: "3px", fontSize: "12px" }}
            >
              <BsCursorFill fontSize={12} />
              <div>Est. : {props.year}</div>
            </div>
          </div>
          <p
            className="mt-3 mb-0 text-center"
            style={{ fontSize: "12px" }}
          >
            <span onClick={() => props.showInfo()} role="button" className="bgprimary rounded-pill text-white px-3 py-2 d-inline-flex align-items-center gap-1">
              {props.type}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-arrow-right"
                viewBox="0 0 16 16"
              >
                <path
                  fillRule="evenodd"
                  d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8"
                />
              </svg>
            </span>
          </p>
        </div>
      </div>
    </>
  );
};

export default RegularSearchCard;
