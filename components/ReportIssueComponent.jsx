import React from "react";

const ReportIssueComponent = (props) => {
  return (
    <div
      className="mt-2 mb-5 d-flex align-items-center justify-content-center"
      style={{ cursor: "pointer", flexDirection: "column", gap: "4px" }}
      onClick={() => props.openModal()}>
      <div>Did we make a mistake?</div>
      <div
        className="px-2 py-1 bgprimary"
        style={{
       
          borderRadius: "6px",
        }}>
        <span style={{ color: "white" }}>Let Us Know</span>
      </div>
    </div>
  );
};

export default ReportIssueComponent;
