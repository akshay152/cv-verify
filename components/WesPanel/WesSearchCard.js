import React from "react";
import styles from "../../styles/searchCard.module.css";

const WesSearchCard = (props) => {
  return (
    <>
      <div
        className={`${styles.searchCard} bg-white mb-3 py-3 border h-100 text-center shadow-sm`}
        style={{ padding: "10px 20px", borderRadius: "12px" }}
      >
        <div className="me-2">
          {props.image ? (
            // eslint-disable-next-line @next/next/no-img-element
            <img
              src={`/images/wes/${props.image}`}
              width={80}
              height={70}
              style={{ objectFit: "contain" }}
              className="rounded"
              alt=""
            />
          ) : (
            // eslint-disable-next-line @next/next/no-img-element
            <img
              src={`/images/university_placeholder.png`}
              width={80}
              height={70}
              style={{ objectFit: "contain" }}
              className="rounded"
              alt=""
            />
          )}
        </div>
        <div className="hello mt-2">
          <div style={{ fontSize: "14px" }}>
            <span className="fw-bold">{props.name}</span>
          </div>
        </div>
      </div>
    </>
  );
};

export default WesSearchCard;
